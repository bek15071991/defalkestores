﻿using System;
using System.ComponentModel; 
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Client.Products;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private double _lastScrollIndex;
        private double _currentScrollIndex;

        public MainPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<MainViewModel>();
            BindingContext = viewModel;
            viewModel.LoadCommand.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            UpdateCategorySelect(); 
        }

        private async void OnCategorySelected(object sender, EventArgs e)
        {
            if (!(e is TappedEventArgs args))
                return; 

            UpdateCategorySelect((View)sender);
            
            var category = (CategoryDto)args.Parameter;
            var viewModel = this.GetVieModel<MainViewModel>();
            viewModel.SelectedCategory = category; 

            await Shell.Current.Navigation.PushAsync(new ProductsPage(category, viewModel.SearchText)); 
        }

        private void UpdateCategorySelect(View? view = null)
        {  
            foreach (var child in categories.Children)
            {
                VisualStateManager.GoToState(child, "Normal");
                ChangeTextColor(child, "#707070");
            }

            if (view != null)
            {
                VisualStateManager.GoToState(view, "Selected");
                ChangeTextColor(view, "#FFFFFF");
            } 

            void ChangeTextColor(View child, string hexColor)
            {
                var txtCtrl = child.FindByName<Label>("CategoryName");

                if (txtCtrl != null)
                    txtCtrl.TextColor = Color.FromHex(hexColor);
            }
        }

        private void OnSearch(object sender, EventArgs e)
        {
            if (!(sender is Entry searchEntry))
                return;

            var viewModel = this.GetVieModel<MainViewModel>();
            viewModel.SearchText = searchEntry.Text;
        }

        private async void OnEndingList(object sender, ItemVisibilityEventArgs e)
        {
            if (!(e.Item is ProductDto product))
                return;

            var viewModel = this.GetVieModel<MainViewModel>();

            await viewModel.PartialLoadProducts(product);
        }

        private async void OnProductSelected(object sender, ItemTappedEventArgs e)
        {
            if (!(e.Item is ProductDto product))
                return;

            await Shell.Current.Navigation.PushAsync(new ProductDetailsPage(product));
        }

        private void  OnSearchTextChanged(object sender, TextChangedEventArgs e)
        {
             if(!string.IsNullOrEmpty(e.NewTextValue))
                 return;

             var viewModel = this.GetVieModel<MainViewModel>();
             viewModel.SearchText = e.NewTextValue;
        }
    }
}