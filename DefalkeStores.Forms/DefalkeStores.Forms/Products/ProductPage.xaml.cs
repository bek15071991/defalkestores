﻿using System;
using System.ComponentModel;
using Autofac;
using Commons;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;


namespace DefalkeStores.Forms.Products
{
    [DesignTimeVisible(false)]
    public partial class ProductPage : ContentPage
    {
        public ProductPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            { 
                BindingContext = scope.Resolve<ProductViewModel>(); 
            }

            MessagingCenter.Subscribe<ProductViewModel, ProductDto>(this, MessageCenterActions.AddedProduct,
                async (view, dto) => { await Navigation.PopAsync(true); });

            MessagingCenter.Subscribe<ProductViewModel, ProductDto>(this, MessageCenterActions.UpdatedProduct,
                async (view, dto) => { await Navigation.PopAsync(true); });

            MessagingCenter.Subscribe<ProductViewModel, int>(this, MessageCenterActions.DeletedProduct,
                async (view, id) => { await Navigation.PopAsync(true); });
        }

        public ProductPage(ProductDto product) : this()
        {
            if (product == null) throw new ArgumentNullException(nameof(product));

            var viewModel = this.GetVieModel<ProductViewModel>();

            viewModel.Load(product);
        }

        private async void Photo_Selected(object sender, EventArgs e)
        {
            var service = DependencyService.Get<IPhotoPickerService>();

            var photo = await service.GetImageAsync();

            if (photo == null)
                return;

            var viewModel = this.GetVieModel<ProductViewModel>();

            viewModel.Photo = photo.Image.ToArray();
            viewModel.PhotoCopy = photo.CopyImage.ToArray();
        }

        private async void OnCategoryChoose(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<ProductViewModel>();

            var categorySelector = new CategorySelectorPage(viewModel.SelectedCategory);

            categorySelector.CategoryChangedEvent += (o, selectedCategory) =>
            {
                viewModel.SelectedCategory = selectedCategory;
            };

            await Navigation.PushAsync(categorySelector, true);
        }
    }
}