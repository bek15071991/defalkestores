﻿namespace DefalkeStores.Forms.Main
{
    public enum MenuItemType
    { 
        Dashboard,
        Categories,
        Products,
        Orders,
        Banners
    }
}