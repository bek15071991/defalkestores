﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace DefalkeStores.Forms.Client.Cart
{
    public interface ICartShoppingStorage
    {
        Task Add(CartLine value);

        Task Remove(CartLine value);

        Task<IReadOnlyCollection<CartLine>> Get();
        Task Clear();
    }

    public class CartShoppingStorage : ICartShoppingStorage
    {
        public const string CartShopping = "cart-shopping";

        public async Task Add(CartLine value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var lines = await GetDictionary();

            lines[value.ProductId] = value;

            await Save(lines);
        }

        public async Task Remove(CartLine value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var lines = await GetDictionary();

            lines.Remove(value.ProductId);

            await Save(lines);
        }

        public async Task<IReadOnlyCollection<CartLine>> Get()
        {
            var lines = await GetDictionary();

            return lines.Values;
        }

        public async Task Clear()
        {
            await SecureStorage.SetAsync(CartShopping, string.Empty);
        }

        private async Task<Dictionary<int, CartLine>> GetDictionary()
        {
            var data = await SecureStorage.GetAsync(CartShopping);

            try
            {
                if (data == null)
                    return new Dictionary<int, CartLine>();

                return JsonConvert.DeserializeObject<Dictionary<int, CartLine>>(data) ??
                       new Dictionary<int, CartLine>();
            }
            catch (JsonSerializationException)
            {
                await Clear();
            }

            return new Dictionary<int, CartLine>();
        }

        private static async Task Save(Dictionary<int, CartLine> lines)
        {
            var data = JsonConvert.SerializeObject(lines);
            await SecureStorage.SetAsync(CartShopping, data);
        }
    }
}