﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using Newtonsoft.Json;

namespace DefalkeStores.Api.Client
{
    public interface ICourierClient
    { 
        Task<CourierSettingsDto> GetSettings();
    }

    public class CourierClient : ICourierClient
    {
        private readonly string _path;
        private readonly HttpClient _httpClient;

        public CourierClient(ClientConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.Url)
            };

            _path = "api/courier";
        }

        public async Task<CourierSettingsDto> GetSettings()
        {
            var response = await _httpClient.GetAsync($"{_path}/settings");

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<CourierSettingsDto>(result);
        }
    }
}