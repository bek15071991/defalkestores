﻿using System;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Categories
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoriesPage : ContentPage
    {
        public CategoriesPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<CategoriesViewModel>();
        }

        private async void OnAddCategory(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CategoryPage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var viewModel = this.GetVieModel<CategoriesViewModel>();

            viewModel.LoadCategoriesCommand.Execute(null);
        }

        private async void OnCategoryTapped(object sender, ItemTappedEventArgs e)
        {
             if(!(e.Item is CategoryDto category))
                 return;

             await Navigation.PushAsync(new CategoryPage(category));
        }
    }
}