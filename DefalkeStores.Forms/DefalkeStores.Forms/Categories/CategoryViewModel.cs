﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Categories
{
    public class CategoryViewModel : ViewModel
    {
        public int Id { get; private set; }

        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _parentName;

        public string ParentName
        {
            get => _parentName;
            set => SetProperty(ref _parentName, value);
        }

        private ObservableCollection<CategoryDto> _children = new ObservableCollection<CategoryDto>();

        public ObservableCollection<CategoryDto> Children
        {
            get => _children;
            set => SetProperty(ref _children, value);
        }

        private int? _parentId;

        public ICommand SaveCommand { get; }
        public ICommand LoadChildrenCommand { get; } 
        public ICommand DeleteCommand { get; }

        private readonly ICategoryClient _categoryClient;
        private readonly IMessageBoxService _messageService;

        public CategoryViewModel(ICategoryClient categoryClient, IMessageBoxService messageService)
        {
            _categoryClient = categoryClient ?? throw new ArgumentNullException(nameof(categoryClient));
            _messageService = messageService ?? throw new ArgumentNullException(nameof(messageService));

            SaveCommand = new Command(Save, () => !string.IsNullOrEmpty(Name) && !IsBusy);
            DeleteCommand = new Command(Delete, () => !IsBusy);
            LoadChildrenCommand = new Command(LoadChildren);
            Title = "Категории";

            MessagingCenter.Subscribe<SubCategoryViewModel>(this, MessageCenterActions.AddedSubCategory,
                view => { LoadChildren(); });
        }

        public void Load(CategoryDto category, string parentName)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            Id = category.Id;
            Name = category.Name;
            _parentId = category.Parent;
            ParentName = parentName;
        }

        private void LoadChildren()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                IsBusy = true;
                try
                {
                    var children = await _categoryClient.GetChildren(Id);

                    Children.Clear();

                    foreach (var child in children)
                    {
                        Children.Add(child);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
                finally
                {
                    IsBusy = false;
                }
            });
        }

        private async void Save()
        {
            try
            {
                IsBusy = true;

                if (Id == 0)
                    await _categoryClient.Create(Name, _parentId);
                else
                    await _categoryClient.Update(Id, Name, _parentId);

                MessagingCenter.Send(this, MessageCenterActions.ChangedCategory + Id);

                Name = null;
                _parentId = null;
                Children?.Clear();
            }
            catch (Exception e)
            {
                await _messageService.Notify(e.Message, "Ошибка");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async void Delete()
        {
            try
            {
                IsBusy = true;

                var isDelete = await _messageService.Ask($"Удалить {Name}?", "Удаление...");

                if (!isDelete)
                    return;

                await _categoryClient.Delete(Id);

                MessagingCenter.Send(this, MessageCenterActions.ChangedCategory + Id);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}