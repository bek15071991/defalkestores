﻿using System;
using DefalkeStores.Api.Dto;

namespace DefalkeStores.Forms.Client.MyOrders
{
    public class OrderView
    {
        public string Number { get; }

        public int Total { get; }

        public int Status { get; }

        public DateTime OnDate { get; }

        public OrderDto Order { get; }

        public OrderView(OrderDto order)
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));

            Number = order.Number;
            Total = order.Total.GetValueOrDefault();
            Status = order.Status.GetValueOrDefault();
            OnDate = order.OnDate.GetValueOrDefault();
        }
    }
}