﻿namespace DefalkeStores.Forms.Helpers
{
    public class MessageCenterActions
    {
        public static string ChangedCategory = nameof(ChangedCategory); 
        public static string AddedProduct = nameof(AddedProduct); 
        public static string UpdatedProduct = nameof(UpdatedProduct); 
        public static string DeletedProduct = nameof(DeletedProduct);

        public static string AddedSubCategory = nameof(AddedSubCategory);

        public static string ChangedOrderStatus = nameof(ChangedOrderStatus);
    }
}