﻿namespace DefalkeStores.Core
{
    public class CourierSettings
    {
        public int Id { get; protected set; }

        public int Cost { get; protected set; }

        public int PostcardCost { get; protected set; }

        protected CourierSettings() // for ef
        {
            
        }

        public CourierSettings(int id, int cost, int postcardCost)
        {
            Id = id;
            Cost = cost;
            PostcardCost = postcardCost;
        }
    }
}