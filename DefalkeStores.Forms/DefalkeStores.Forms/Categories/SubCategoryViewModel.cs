﻿using System;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Categories
{
    public class SubCategoryViewModel : ViewModel
    {
        private int _parentId;

        private string _parentName;

        public string ParentName
        {
            get => _parentName;
            set => SetProperty(ref _parentName, value);
        }

        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public ICommand SaveCommand { get; }

        private readonly ICategoryClient _categoryClient;
        private readonly IMessageBoxService _messageService;

        public SubCategoryViewModel(ICategoryClient categoryClient, IMessageBoxService messageService)
        {
            _categoryClient = categoryClient ?? throw new ArgumentNullException(nameof(categoryClient));
            _messageService = messageService ?? throw new ArgumentNullException(nameof(messageService));

            SaveCommand = new Command(Save, () => !string.IsNullOrEmpty(ParentName) && !IsBusy);
            Title = "Подкатегория";
        }

        public void Load(int parentId, string parentName)
        {
            _parentId = parentId;
            ParentName = parentName;
        }

        private async void Save()
        {
            try
            {
                IsBusy = true;

                await _categoryClient.Create(Name, _parentId);

                MessagingCenter.Send(this, MessageCenterActions.AddedSubCategory);
          
            }
            catch (Exception e)
            {
                await _messageService.Notify(e.Message, "Ошибка");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}