﻿namespace DefalkeStores.Api.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Cost { get; set; }

        public int Price { get; set; }

        public int Count { get; set; }

        public string Description { get; set; }

        public byte[] Image { get; set; }

        public int Category { get; set; }
    }
}