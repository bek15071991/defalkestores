﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;

namespace DefalkeStores.Forms.Client.MyOrders
{
    public class MyOrderDetailsViewModel : ViewModel
    {
        private string _orderNumber;

        public string OrderNumber
        {
            get => _orderNumber;
            set => SetProperty(ref _orderNumber, value);
        }

        private OrderStatus _orderStatus;

        public OrderStatus OrderStatus
        {
            get => _orderStatus;
            set => SetProperty(ref _orderStatus, value);
        }

        private string _destinationName;

        public string DestinationName
        {
            get => _destinationName;
            set => SetProperty(ref _destinationName, value);
        }

        private string _destinationAddress;

        public string DestinationAddress
        {
            get => _destinationAddress;
            set => SetProperty(ref _destinationAddress, value);
        }

        private string _destinationNumber;

        public string DestinationNumber
        {
            get => _destinationNumber;
            set => SetProperty(ref _destinationNumber, value);
        }

        private bool _includePostCard;

        public bool IncludePostCard
        {
            get => _includePostCard;
            set => SetProperty(ref _includePostCard, value);
        }

        private string _postCardText;

        public string PostCardText
        {
            get => _postCardText;
            set => SetProperty(ref _postCardText, value);
        }

        private int _postCardCost; 
        public int PostCardCost
        {
            get => _postCardCost;
            set => SetProperty(ref _postCardCost, value);
        } 

        public ObservableCollection<OrderLineDto> Lines { get; set; }

        private int _amount;

        public int Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private int _courierCost;

        public int CourierCost
        {
            get => _courierCost;
            set => SetProperty(ref _courierCost, value);
        }

        private int _total;

        public int Total
        {
            get => _total;
            set => SetProperty(ref _total, value);
        }

        public void Load(OrderDto order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            OrderNumber = order.Number;
            OrderStatus = (OrderStatus) order.Status.GetValueOrDefault();
            DestinationAddress = order.DestinationAddress;
            DestinationName = order.DestinationName;
            DestinationNumber = order.DestinationPhone;
            PostCardText = order.PostcardText;
            IncludePostCard = order.Postcard != null;
            PostCardCost = order.Postcard.GetValueOrDefault();

            Lines = new ObservableCollection<OrderLineDto>(order.Lines);

            Amount = Lines.Sum(s => s.Price.GetValueOrDefault());
            Total = order.Total.GetValueOrDefault();
            CourierCost = order.CourierCost.GetValueOrDefault();
        }
    }
}