﻿using DefalkeStores.Application.Configurations;
using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public class StoresContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<User> Users { get; set; }
         
        public DbSet<Banner> Banners { get; set; }
         
        public DbSet<CourierSettings> CourierSettings { get; set; }

        public StoresContext(DbContextOptions<StoresContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            modelBuilder.ApplyConfiguration(new CategoryConfig());
            modelBuilder.ApplyConfiguration(new ProductConfig());
            modelBuilder.ApplyConfiguration(new OrderConfig());
            modelBuilder.ApplyConfiguration(new OrderLineConfig());
            modelBuilder.ApplyConfiguration(new OrderLineConfig());
            modelBuilder.ApplyConfiguration(new BannerConfig());

            modelBuilder.Entity<CourierSettings>().HasData(new CourierSettings(1,120, 100)); 
            modelBuilder.Entity<Banner>().HasData(new Banner(1, "По умолчанию","Здесь может быть ваша реклама", null, null, null)); 
        }
    }
}