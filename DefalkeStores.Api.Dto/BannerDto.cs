﻿namespace DefalkeStores.Api.Dto
{
    public class BannerDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Text { get; set; }

        public byte[] Image1 { get; set; }
        public byte[] Image2 { get; set; }
        public byte[] Image3 { get; set; }
    }
}