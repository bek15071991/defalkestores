﻿using System;
using System.Collections.Generic;
using System.Net.Http; 
using System.Text;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using Newtonsoft.Json;

namespace DefalkeStores.Api.Client
{
    public interface ICategoryClient
    {
        Task<CategoryDto> Find(int id);

        Task<IList<CategoryDto>> Get(string? name = null, int count = 10);

        Task Create(string name, int? parentId);

        Task Update(int id, string name, int? parentId = null);

        Task Delete(int id);

        Task<IList<CategoryDto>> GetChildren(int id);

        Task<IList<CategoryDto>> GetRoot( );
    }

    public class CategoryClient : ICategoryClient
    {
        private readonly string _path;
        private readonly HttpClient _httpClient;

        public CategoryClient(ClientConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.Url)
            }; 

            _path = "api/categories";
        }

        public async Task<CategoryDto> Find(int id)
        {
            var url = $"{_path}/{id}"; 

            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<CategoryDto>(result);
        }

        public async Task<IList<CategoryDto>> Get(string? name, int count = 10)
        {
            var url = $"{_path}?count={count}";

            if (!string.IsNullOrEmpty(name))
                url += $"&{nameof(name)}={name}";

            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<CategoryDto>>(result);
        }

        public async Task Create(string name, int? parentId)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var content = JsonConvert.SerializeObject(new
            {
                name,
                parentId
            });

            var result =
                await _httpClient.PostAsync(_path, new StringContent(content, Encoding.UTF8, "application/json"));

            result.EnsureSuccessStatusCode();
        } 

        public async Task Update(int id, string name, int? parentId = null)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var content = JsonConvert.SerializeObject(new
            {
                name,
                parentId
            });

            var result =
                await _httpClient.PutAsync($"{_path}/{id}", new StringContent(content, Encoding.UTF8, "application/json"));
            result.EnsureSuccessStatusCode();
        }

        public async Task Delete(int id)
        {
            var result = await _httpClient.DeleteAsync($"{_path}/{id}");
            result.EnsureSuccessStatusCode();
        }

        public async Task<IList<CategoryDto>> GetChildren(int id)
        {
            var url = $"{_path}/{id}/children";

            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<CategoryDto>>(result);
        }

        public async Task<IList<CategoryDto>> GetRoot()
        {
            var url = $"{_path}/root";

            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<CategoryDto>>(result);
        }
    }
}