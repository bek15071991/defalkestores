﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DefalkeStores.Core;

namespace DefalkeStores.Application
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
        }

        public async Task Save(string name, int cost, int price, int count, string? description, byte[] image, Category category)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            if (image == null) 
                throw new ArgumentNullException(nameof(image));

            if (category == null) 
                throw new ArgumentNullException(nameof(category));

            Product product = null;// new Product(name, cost, price, count, description,);

            await _productRepository.Save(product);
        }

        public Task<IList<Product>> Get(string name)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Task Update(string name, int cost, int price, int count, string description, byte[] image, Category category)
        {
            throw new NotImplementedException();
        } 
    }
}