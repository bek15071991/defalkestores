﻿using System;
using Commons.DataAccess;

namespace DefalkeStores.Application
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly StoresContext _storesContext;

        public UnitOfWorkFactory(StoresContext storesContext)
        {
            _storesContext = storesContext ?? throw new ArgumentNullException(nameof(storesContext));
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork(_storesContext);
        }
    }
}