﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;
using Microsoft.AspNetCore.Mvc;

namespace DefalkeStores.Api.Categories
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public async Task<IList<CategoryDto>> Get([FromQuery] int count, int? category = null, string? name = null)
        {
            var categories = await _categoryRepository.Get(name, count);

            return categories.Select(c => new CategoryDto
            {
                Id = c.Id,
                Name = c.Name,
                Parent = c.ParentId
            }).ToList();
        }
       
        [HttpGet("{id}/children")]
        public async Task<IActionResult> GetChildren(int id)
        {
            var categories = await _categoryRepository.GetChildren(id);

            return Ok(categories?.Select(f => new CategoryDto
            {
                Id = f.Id,
                Name = f.Name,
                Parent = f.ParentId
            }));
        }

        [HttpGet("root")]
        public async Task<IActionResult> GetRoot()
        {
            var categories = await _categoryRepository.GetRoot();

            return Ok(categories?.Select(f => new CategoryDto
            {
                Id = f.Id,
                Name = f.Name,
                Parent = f.ParentId
            }));
        }

        [HttpGet("{id}")]
        public async Task<CategoryDto> Find(int id)
        {
            var category = await _categoryRepository.Find(id);

            return new CategoryDto
            {
                Id = category.Id,
                Name = category.Name,
                Parent = category.ParentId
            };
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryModel model)
        {
            if (model == null)
                return BadRequest();

            var parentId = model.ParentId;
            var name = model.Name;

            Category category = null;

            if (parentId != null)
            {
                var parent = await _categoryRepository.Find(parentId.Value);

                if (parent == null)
                    BadRequest("ParentId is invalid");

                category = new Category(name, parent);
            }

            category ??= new Category(name);

            await _categoryRepository.Save(category);

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CategoryModel model)
        {
            if (model == null)
                return BadRequest();

            var category = await _categoryRepository.Find(id);

            if (category == null)
                return BadRequest();

            category.SetName(model.Name);

            if (model.ParentId != null)
            {
                var parent = await _categoryRepository.Find(model.ParentId.Value);

                if (parent == null)
                    return BadRequest();

                category.SetParent(parent);
            }

            await _categoryRepository.Save(category);

            return Ok();
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var category = await _categoryRepository.Find(id);

            if (category == null)
                return BadRequest();

            await _categoryRepository.Delete(category);

            return Ok();
        }
    }
}