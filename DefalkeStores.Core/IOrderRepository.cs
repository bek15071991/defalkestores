﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Commons.DataAccess;
using DefalkeStores.Api.Dto;

namespace DefalkeStores.Core
{
    public interface IOrderRepository : ISaveRepository<Order>
    {
        Task<IList<Order>> Get(int count, int skip, int? userId = null, OrderStatus? orderStatus = null);
        Task<IList<Order>> GetActive(int count, int skip);
        Task<Order?> Find(int id);
    }
}