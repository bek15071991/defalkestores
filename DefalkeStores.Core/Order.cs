﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commons.DataAccess;
using DefalkeStores.Api.Dto;

namespace DefalkeStores.Core
{
    public class Order : IEntity
    {
        public int Id { get; protected set; }

        public string Number => $"OR-{Id}";

        public string DestinationAddress { get; protected set; }
        public string DestinationName { get; protected set; }
        public string DestinationPhone { get; protected set; }
        public int? Postcard { get; protected set; }
        public string? PostcardText { get; protected set; }
        public int CourierCost { get; protected set; }
        public DateTime OnDate { get; protected set; }
        public OrderStatus Status { get; protected set; }
        public User User { get; protected set; }
        public int UserId { get; protected set; }
        public int Total => CourierCost + Postcard.GetValueOrDefault() + Lines.Sum(l => l.Count * l.Product.Price);

        private readonly IList<OrderLine> _lines = new List<OrderLine>();
        public IEnumerable<OrderLine> Lines => _lines;

        protected Order() // ef
        {
        }

        public Order(User user,
            string destinationAddress,
            string destinationName,
            string destinationPhone,
            int courierCost,
            int? postcard,
            string? postcardText)
        {
            User = user ?? throw new ArgumentNullException(nameof(user));

            DestinationAddress = destinationAddress ?? throw new ArgumentNullException(nameof(destinationAddress));
            DestinationName = destinationName ?? throw new ArgumentNullException(nameof(destinationName));
            DestinationPhone = destinationPhone ?? throw new ArgumentNullException(nameof(destinationPhone));
            CourierCost = courierCost;
            Postcard = postcard;
            PostcardText = postcardText;
            UserId = user.Id;

            OnDate = DateTime.Now;
            Status = OrderStatus.PaymentPending;
        }

        public void AddLine(Product product, int count)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));

            _lines.Add(new OrderLine(product, count));
        }

        public void UpdateStatus(OrderStatus status)
        {
            if (Status == status)
                return;

            if (status == OrderStatus.Canceled)
            {
                foreach (var orderLine in Lines)
                {
                    var product = orderLine.Product;

                    product.Increase(orderLine.Count);
                }
            }
            else if (Status == OrderStatus.Canceled)
            {
                foreach (var orderLine in Lines)
                {
                    var product = orderLine.Product;

                    if (product.Count < orderLine.Count)
                        throw new InvalidOperationException($"Не осталось товаров#{product.Id}");

                    product.Decrease(orderLine.Count);
                }
            }

            Status = status;
        }
    }
}