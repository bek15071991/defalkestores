﻿namespace Commons.DataAccess
{
    public interface IEntity
    {
        int Id { get; }
    }
}