﻿using System;
using Commons.DataAccess;
using static Commons.ImageHelper;

namespace DefalkeStores.Core
{
    public class Product : IEntity
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; }

        public int Cost { get; protected set; }

        public int Price { get; protected set; }

        public int Count { get; protected set; }

        public string Description { get; protected set; }

        public byte[] ThumbnailImage { get; protected set; }

        public ProductOriginalImage OriginalImage { get; protected set; }

        public Category Category { get; protected set; }

        public int CategoryId { get; protected set; }

        protected Product() // for ef
        {
        }

        public Product(string name,
            int cost,
            int price,
            int count,
            string description,
            byte[] image,
            Category category)
        {
            Set(name, cost, price, count, description, image, category);
        }

        public void Set(string name,
            int cost,
            int price,
            int count,
            string description,
            byte[] image,
            Category category)
        {
            if (image == null)
                throw new ArgumentNullException(nameof(image));

            Name = name ?? throw new ArgumentNullException(nameof(name));
            Cost = cost;
            Price = price;
            Count = count;
            Description = description;
            Category = category ?? throw new ArgumentNullException(nameof(category));
            CategoryId = Category.Id;

            var lightImage = MakeLightImage(image);

            if (OriginalImage == null)
                OriginalImage = new ProductOriginalImage(this, lightImage);
            else
                OriginalImage.Update(image);

            ThumbnailImage = MakeThumbnail(image);
        }

        public void Decrease(int count)
        {
            Count -= count;
        }

        public void Increase(int count)
        {
            Count += count;
        }
    }
}