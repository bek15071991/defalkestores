﻿using System;
using System.Threading.Tasks;
using Commons.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public abstract class RepositoryBase<T> : IDeleteRepository<T>, ISaveRepository<T> where T : IEntity
    {
        protected readonly StoresContext StoresContext;

        protected RepositoryBase(StoresContext storesContext)
        {
            StoresContext = storesContext ?? throw new ArgumentNullException(nameof(storesContext));
        }

        public async Task<T> Save(T t)
        {
            var isNew = t.Id == 0;

            if (isNew)
                await StoresContext.AddAsync(t);
            else StoresContext.Update(t);

            await StoresContext.SaveChangesAsync();
            return t;
        }

        public async Task Delete(T t)
        {
            StoresContext.Entry(t).State = EntityState.Deleted;

            await StoresContext.SaveChangesAsync();
        }
    }
}