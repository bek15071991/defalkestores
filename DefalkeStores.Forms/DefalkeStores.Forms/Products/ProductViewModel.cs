﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input; 
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Helpers; 
using Xamarin.Forms;

namespace DefalkeStores.Forms.Products
{
    public class ProductViewModel : ViewModel
    {
        private int _id; 

        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private int _cost;

        public int Cost
        {
            get => _cost;
            set => SetProperty(ref _cost, value);
        }

        private int _price;

        public int Price
        {
            get => _price;
            set => SetProperty(ref _price, value);
        }

        private int _count; 
        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }

        private string _description;

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        private byte[] _photo;

        public byte[] Photo
        {
            get => _photo;
            set => SetProperty(ref _photo, value);
        }

        private byte[] _photoCopy;

        public byte[] PhotoCopy
        {
            get => _photoCopy;
            set => SetProperty(ref _photoCopy, value);
        }

        private bool _hasCategory;
        public bool HasCategory
        {
            get => _hasCategory;
            set => SetProperty(ref _hasCategory, value);
        } 

        private CategoryDto _selectedCategory; 
        public CategoryDto SelectedCategory
        {
            get => _selectedCategory;
            set => SetProperty(ref _selectedCategory, value, onChanged: () => { HasCategory = value != null; });
        }

        public ICommand SaveCommand { get; }
        public ICommand DeleteCommand { get; }


        private readonly ICategoryClient _categoryClient;
        private readonly IProductClient _productClient;
        private readonly IMessageBoxService _messageService; 

        public ProductViewModel(ICategoryClient categoryClient,
            IProductClient productClient,
            IMessageBoxService messageService)
        {
            _categoryClient = categoryClient ?? throw new ArgumentNullException(nameof(categoryClient));
            _productClient = productClient ?? throw new ArgumentNullException(nameof(productClient));
            _messageService = messageService ?? throw new ArgumentNullException(nameof(messageService));
            Title = "Товары";

            SaveCommand = new Command(async () => await Save(), CanExecute);
            DeleteCommand = new Command(async () => await Delete(), () => _id != 0);
        } 

        public void Load(ProductDto product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            HasCategory = true;

            Device.BeginInvokeOnMainThread(async () =>
            {
                IsBusy = true; 

                SelectedCategory = await _categoryClient.Find(product.Category);
                Photo = product.Image;

                IsBusy = false;
            });

            _id = product.Id;
            Name = product.Name;
            Cost = product.Cost;
            Price = product.Price;
            Count = product.Count;
            Description = product.Description;
            Photo = product.Image;
        }

        private async Task Save()
        {
            var product = new ProductDto
            {
                Category = SelectedCategory.Id,
                Name = Name,
                Count = Count,
                Cost = Cost,
                Description = Description,
                Price = Price,
                Image = PhotoCopy ?? Photo
            }; 

            try
            {
                IsBusy = true;

                if (_id == 0)
                {
                    await _productClient.Create(product);
                    MessagingCenter.Send(this, MessageCenterActions.AddedProduct, product);
                }
                else
                {
                    await _productClient.Update(_id, product);
                    product.Id = _id;
                    MessagingCenter.Send(this, MessageCenterActions.UpdatedProduct, product);
                }
            }
            catch (Exception e)
            {
                await _messageService.Notify(e.Message, "Ошибка");
            }
            finally
            {
                Reset();
                IsBusy = false;
            }
        }

        private async Task Delete()
        {
            if (_id == 0)
                return;

            try
            {
                var isDelete = await _messageService.Ask($"Удалить {Name}?", "Удаление...");

                if (!isDelete)
                    return;

                await _productClient.Delete(_id);

                MessagingCenter.Send(this, MessageCenterActions.DeletedProduct, _id);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        private bool CanExecute()
        {
            return !IsBusy &&
                   SelectedCategory != null &&
                   Count > 0 &&
                   Cost > 0 &&
                   Price > 0 &&
                   Photo != null &&
                   !string.IsNullOrEmpty(Name);
        }

        private void Reset()
        {
            Name = null;
            Count = Cost = Price = 0;
            Photo = null;
            Description = null;
        }
    }
}