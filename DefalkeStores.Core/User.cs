﻿using System;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public class User : IEntity
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; } 

        public string Phone { get; protected set; }

        public User(string name, string phone)
        { 
            Name = name ?? throw new ArgumentNullException(nameof(name)); 
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
        }

        public void Update(string name, string phone)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
        }
    }
}