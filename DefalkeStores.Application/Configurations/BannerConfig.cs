﻿using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DefalkeStores.Application.Configurations
{
    class BannerConfig : IEntityTypeConfiguration<Banner>
    {
        public void Configure(EntityTypeBuilder<Banner> builder)
        {

            builder.ToTable("Banners");
            builder.Property(f => f.Text).HasMaxLength(100);
            builder.Property(f => f.Name).HasMaxLength(30);
            builder.Property(f => f.Image1).IsRequired(false);
            builder.Property(f => f.Image2).IsRequired(false);
            builder.Property(f => f.Image3).IsRequired(false);
        }
    }
}
