﻿using System;

namespace DefalkeStores.Core
{
    public class ProductOriginalImage
    {
        public int Id { get; protected set; }

        public byte[] Image { get; protected set; }

        public Product Product { get; protected set; }

        public int ProductId { get; protected set; }

        protected ProductOriginalImage() // for ef
        {
        }

        public ProductOriginalImage(Product product, byte[] image)
        {
            Product = product ?? throw new ArgumentNullException(nameof(product));
            Image = image ?? throw new ArgumentNullException(nameof(image));
            ProductId = product.Id;
        }

        public void Update(byte[] image)
        {
            Image = image ?? throw new ArgumentNullException(nameof(image));
        }
    }
}