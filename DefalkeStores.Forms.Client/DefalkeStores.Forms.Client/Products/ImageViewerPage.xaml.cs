﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageViewerPage : ContentPage
    {
        public ObservableCollection<byte[]> Images { get; set; }

        public ImageViewerPage()
        {
            InitializeComponent(); 
        }

        public ImageViewerPage(IList<byte[]> images) : this()
        {
            Images = new ObservableCollection<byte[]>(images);
            CarouselView.ItemsSource = images.Select(f => ImageSource.FromStream(() => new MemoryStream(f.ToArray())));
        }

        private void OnGoBack(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
