﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Cart
{
    public class CartShoppingViewModel : ViewModel
    {
        private int _amount;

        public int Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private bool _canMakeOrder;

        public bool CanMakeOrder
        {
            get => _canMakeOrder;
            set => SetProperty(ref _canMakeOrder, value);
        }

        public ObservableCollection<CartLine> Lines { get; } = new ObservableCollection<CartLine>();

        public ICommand DeleteCommand => new Command<CartLine>(DeleteLine);

        private readonly ICartShoppingStorage _cartShoppingStorage;

        public ICommand LoadCommand { get; }

        public CartShoppingViewModel(ICartShoppingStorage cartShoppingStorage)
        {
            _cartShoppingStorage = cartShoppingStorage ?? throw new ArgumentNullException(nameof(cartShoppingStorage));

            MessagingCenter.Subscribe<MakeOrderViewModel>(this, MessageCenterActions.OrderCompleted, async (v) =>
            {
                Amount = 0;
                await _cartShoppingStorage.Clear();
                Lines.Clear();
                CanMakeOrder = false;
            });

            LoadCommand = new Command(async () => await Load());
        }

        private async Task Load()
        {
            var lines = await _cartShoppingStorage.Get();

            if (lines == null)
                return;

            Lines.Clear();

            foreach (var cartLine in lines)
            {
                cartLine.PropertyChanged += CartLineCountChanged;

                Lines.Add(cartLine);
            }

            CalculateAmount();
        }

        public async Task Add(ProductDto product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            var cartLine = Lines.FirstOrDefault(l => l.ProductId == product.Id);

            if (cartLine == null)
            {
                cartLine = new CartLine
                {
                    ProductId = product.Id,
                    ProductName = product.Name,
                    ProductPrice = product.Price,
                    RemainderOfProducts = product.Count,
                    ProductImage = product.Image
                };

                cartLine.PropertyChanged += CartLineCountChanged;

                Lines.Add(cartLine);
            }

            cartLine.IncreaseCommand.Execute(cartLine);

            await _cartShoppingStorage.Add(cartLine);
        }

        private async void CartLineCountChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName != "Count"
                || !(sender is CartLine cartLine))
                return;

            CalculateAmount();

            await _cartShoppingStorage.Add(cartLine);
        }

        private async void DeleteLine(CartLine cartLine)
        {
            cartLine.PropertyChanged -= CartLineCountChanged;

            Lines.Remove(cartLine);

            await _cartShoppingStorage.Remove(cartLine);

            CalculateAmount();
        }

        private void CalculateAmount()
        {
            Amount = Lines.Sum(s => s.ProductPrice * s.Count);

            CanMakeOrder = Lines.Any();
        }

        public OrderDto MakeOrder()
        {
            var order = new OrderDto
            {
                Lines = Lines.Select(l => new OrderLineDto
                {
                    Count = l.Count,
                    ProductId = l.ProductId
                }).ToList()
            };

            return order;
        }
    }
}