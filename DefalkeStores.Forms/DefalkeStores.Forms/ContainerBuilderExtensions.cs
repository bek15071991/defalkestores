﻿using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Forms.Banners;
using DefalkeStores.Forms.Categories;
using DefalkeStores.Forms.Dashboard;
using DefalkeStores.Forms.Orders;
using DefalkeStores.Forms.Products;

namespace DefalkeStores.Forms
{
   public static class ContainerBuilderExtensions
    {
        public static void RegisterAll(this ContainerBuilder builder)
        {
            builder.RegisterModule<ApiClientModule>();

            builder.RegisterType<MessageBoxService>().As<IMessageBoxService>(); 

            builder.RegisterType<DashboardViewModel>();

            builder.RegisterType<CategoryViewModel>(); 
            builder.RegisterType<CategoriesViewModel>();
            builder.RegisterType<SubCategoryViewModel>();

            builder.RegisterType<ProductViewModel>(); 
            builder.RegisterType<ProductsViewModel>();

            builder.RegisterType<OrdersViewModel>();
            builder.RegisterType<OrderDetailsViewModel>();

            builder.RegisterType<CategorySelectorViewModel>();
            builder.RegisterType<BannerViewModel>();

            const string url = "http://20.123.175.118:5005"; // "http://176.123.246.252:5005"; //"http://192.168.1.109"; //

            builder.RegisterInstance(new ClientConfiguration{ Url = url });  
        }
    }
}
