﻿using System;
using System.Collections.Generic;

namespace DefalkeStores.Api.Dto
{
    public class OrderDto
    {
        public int? Id { get; set; }
        public UserDto User { get; set; }  
        public string? Number { get; set; } 
        public DateTime? OnDate { get; set; }
        public string DestinationAddress { get; set; }
        public string DestinationName { get; set; }
        public string DestinationPhone { get; set; }
        public int? Postcard { get; set; } 
        public string? PostcardText { get; set; } 
        public int? Total { get; set; } 
        public int? Status { get; set; } 
        public int? CourierCost { get; set; }
        public List<OrderLineDto> Lines { get; set; }
    }
}