﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace Commons.Forms
{
    public interface IUserService<T>
    {
        Task SetCurrentUser<T>(T userProfile);

        Task<T> GetCurrentUser();
    }

    public class UserService<T> : IUserService<T>
    {
        public const string CurrentUser = "current-user";

        public Task SetCurrentUser<T>(T userProfile)
        {
            if (userProfile == null) 
                throw new ArgumentNullException(nameof(userProfile));

            var data = JsonConvert.SerializeObject(userProfile);
            return SecureStorage.SetAsync(CurrentUser, data);
        }

        public async Task<T> GetCurrentUser()
        {
            var data = await SecureStorage.GetAsync(CurrentUser);

            return data == null ? default : JsonConvert.DeserializeObject<T>(data);
        }
    }
}