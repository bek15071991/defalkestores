﻿using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Main
{ 
    [DesignTimeVisible(false)]
    public partial class MenuPage
    {
        private static MainPage RootPage => Application.Current.MainPage as MainPage;
        IList<HomeMenuItem> menuItems;

        public MenuPage()
        {
            InitializeComponent();

            menuItems = MenuFactory.Create();

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = ((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }
    }
}