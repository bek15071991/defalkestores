﻿using DefalkeStores.Api.Dto;

namespace DefalkeStores.Api.Orders
{
    public static class OrderGuard
    {
        public static bool Validate(OrderDto orderDto)
        {
            return !string.IsNullOrEmpty(orderDto.DestinationAddress) &&
                   !string.IsNullOrEmpty(orderDto.DestinationName) &&
                   !string.IsNullOrEmpty(orderDto.DestinationPhone);
        }
    }
}