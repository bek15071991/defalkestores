﻿using System;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Commons.Forms
{
    public abstract class ViewModel : ObservableObject
    {    
        private bool _isBusy = false;

        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        private string _title = string.Empty;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
         
        protected override bool SetProperty<T>(ref T backingStore, T value, string propertyName = "", Action onChanged = null)
        {
            var result = base.SetProperty(ref backingStore, value, propertyName, onChanged);
            ChangeCanExecute();

            return result;
        }

        private void ChangeCanExecute()
        {
            var commandInfos = this.GetType().GetProperties()
                .Where(p => p.PropertyType == typeof(ICommand))
                .ToList();

            foreach (var commandInfo in commandInfos)
            {
                var command = commandInfo.GetValue(this, null ) as Command;

                command?.ChangeCanExecute(); 
            }
        } 
    }
}