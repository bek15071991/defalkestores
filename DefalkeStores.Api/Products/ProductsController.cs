﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using DefalkeStores.Application;
using DefalkeStores.Core;
using Microsoft.AspNetCore.Mvc;

namespace DefalkeStores.Api.Products
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;

        public ProductsController(IProductRepository productRepository, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
            _categoryRepository = categoryRepository ?? throw new ArgumentNullException(nameof(categoryRepository));
        }

        [HttpGet]
        public async Task<IList<ProductDto>> Get(int count, int? category = null, string? name = null, int skip = 0)
        {
            var products = await _productRepository.Get(category, name, skip, count);

            return products.Select(p => new ProductDto
                {
                    Id = p.Id,
                    Name = p.Name,
                    Category = p.CategoryId,
                    Count = p.Count,
                    Price = p.Price,
                    Cost = p.Cost,
                    Description = p.Description,
                    Image = p.ThumbnailImage
                })
                .ToList();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductDto model)
        {
            if (model == null)
                return BadRequest("Parameters is empty");

            var category = await _categoryRepository.Find(model.Category);

            if (category == null)
                return BadRequest("Category is invalid");

            var product = new Product(model.Name,
                model.Cost,
                model.Price,
                model.Count,
                model.Description,
                model.Image,
                category);

            await _productRepository.Save(product);

            return Ok();
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ProductDto model)
        {
            if (model == null)
                return BadRequest("Parameters is empty");

            var product = await _productRepository.Find(id, includeImage: true);

            if (product == null)
                return BadRequest("Product is not found");

            var category = await _categoryRepository.Find(model.Category);

            if (category == null)
                return BadRequest("Category is invalid");

            product.UpdateFrom(model, category);

            await _productRepository.Save(product);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var product = await _productRepository.Find(id);

            if (product == null)
                return BadRequest("Product is not found");

            await _productRepository.Delete(product);

            return Ok();
        }

        [HttpGet("{id}/image")]
        public async Task<IActionResult> Get(int id)
        {
            var product = await _productRepository.Find(id, true);

            if (product == null)
                return BadRequest();
            
            var image = product.OriginalImage.Image;

            return File(image, "image/jpeg");
        }
    }
}