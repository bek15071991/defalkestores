﻿using System.IO;

namespace DefalkeStores.Forms.Products
{
   public class ProductImage
    { 
        public Stream Image { get; set; }
        public Stream CopyImage { get; set; }
    }
}
