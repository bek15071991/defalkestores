﻿using System;
using System.Threading.Tasks;
using Autofac;
using Commons.Forms;
using DefalkeStores.Forms.Main;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace DefalkeStores.Forms
{
    public partial class App
    {
        public static IContainer Container; 

        public App()
        {
            Device.SetFlags(new[] { "SwipeView_Experimental" });

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("Mzg3MzAzQDMxMzgyZTM0MmUzMGNxZFZZV3U5SDJHWFpYQ1ZXaTM4UG8zUDI2VlRwR1JHbXRsd3I4VmpxMnM9");

            InitializeComponent();

            HandlingException();

            var builder = new ContainerBuilder(); 
            builder.RegisterAll();

            Container = builder.Build(); 

            DependencyResolver.ResolveUsing(type => Container.IsRegistered(type) ? Container.Resolve(type) : null);

            MainPage = new MainPage();
        }

        private void HandlingException()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => ShowException((Exception)args.ExceptionObject);
            TaskScheduler.UnobservedTaskException += (sender, args) => ShowException(args.Exception);
        }

        private void ShowException(Exception ex)
        {
            var messageService = Container.Resolve<IMessageBoxService>();

            messageService.Notify(ex.Message, "Системная ошибка");
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}