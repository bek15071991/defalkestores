﻿using Xamarin.Forms;

namespace Commons.Forms
{
    public static class PageHelper
    {
        public static T GetVieModel<T>(this ContentPage contentPage) where T : ViewModel
        {
            return (T) contentPage.BindingContext;
        }
    }
}