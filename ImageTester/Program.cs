﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Commons;

namespace ImageTester
{
    class Program
    {
        static void Main(string[] args)
        {
            var image = File.ReadAllBytes("photo.jpg");

            var thumbnailImage = ImageHelper.MakeThumbnail(image);

            using (var newBitmap = new Bitmap(new MemoryStream(thumbnailImage)))
            {
                var name = $"tumbnail {DateTime.Now.ToString("h-mm-ss")}.jpg";
                newBitmap.Save(name, ImageFormat.Jpeg);
            } 

            var newImage = ImageHelper.MakeLightImage(image);

            using (var newBitmap = new Bitmap(new MemoryStream(newImage)))
            {
                var name = $"newImage {DateTime.Now.ToString("h-mm-ss")}.jpg";
                newBitmap.Save(name, ImageFormat.Jpeg);
            }

            Console.WriteLine("saved");
            Console.Read();
        }
    }
}