﻿using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DefalkeStores.Application.Configurations
{
    class OrderConfig : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Ignore(o => o.Number);
            builder.Ignore(o => o.Total);
            builder.Navigation(o => o.Lines).AutoInclude(true);
        }
    }
}