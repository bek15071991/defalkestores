﻿using System; 
using System.Threading.Tasks;
using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public class CourierSettingsProvider : ICourierSettingsProvider
    {
        protected readonly StoresContext StoresContext;

        public CourierSettingsProvider(StoresContext storesContext)
        {
            StoresContext = storesContext ?? throw new ArgumentNullException(nameof(storesContext));
        }

        public async Task<CourierSettings> Get()
        {
            return await StoresContext.CourierSettings.FirstAsync();
        }
    }
}