﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DefalkeStores.Core
{
   public interface IProductService
   {
       Task Save(string name, int cost, int price, int count, string? description, byte[] image, Category category);

       Task<IList<Product>> Get(string name);

       Task Remove(int id);

       Task Update(string name, int cost, int price, int count, string? description, byte[] image, Category category);
   }
}
