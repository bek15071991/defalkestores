﻿using System;
using Autofac;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.MyOrders
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyOrderDetailsPage : ContentPage
    {
        public MyOrderDetailsPage(OrderDto order)
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<MyOrderDetailsViewModel>();
            BindingContext = viewModel;
            viewModel.Load(order);
        }

        private async void OnBack(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync(true);
        }
    }
}