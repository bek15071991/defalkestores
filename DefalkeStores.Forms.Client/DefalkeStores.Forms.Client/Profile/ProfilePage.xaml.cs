﻿using System; 
using Autofac;
using Commons.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Profile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {  
        public ProfilePage()
        {
            InitializeComponent();
            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<UserProfileViewModel>();

            BindingContext = viewModel;

            viewModel.LoadCommand.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var viewModel = this.GetVieModel<UserProfileViewModel>();

            viewModel.LoadCommand.Execute(null);
        }

        private async void OnShowMyOrders(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//MyOrders");
        }

        private async void OnShowCartShopping(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//CartShopping");
        }

        private async void OnEditProfile(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new EditProfilePage(), true);
        }

        private async void OnShowPaymentAndDelivery(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new PaymentAndDeliveryPage(), true);
        }

        private async void OnShowPartners(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new PartnersPage());
        }
    }
}