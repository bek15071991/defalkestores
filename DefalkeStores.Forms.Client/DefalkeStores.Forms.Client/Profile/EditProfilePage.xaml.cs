﻿using System; 
using Autofac;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Profile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfilePage
    {  
        public EditProfilePage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<EditProfileViewModel>();

            BindingContext = viewModel;

            viewModel.LoadCommand.Execute(null);
        }

        private async void OnBack(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync(true);
        }
    }
}