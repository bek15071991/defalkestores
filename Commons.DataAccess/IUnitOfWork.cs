﻿using System;
using System.Threading.Tasks;

namespace Commons.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        Task Commit();
    }
}