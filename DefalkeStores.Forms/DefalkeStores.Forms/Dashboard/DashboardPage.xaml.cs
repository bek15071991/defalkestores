﻿using System.ComponentModel;
using Autofac;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Dashboard
{ 
    [DesignTimeVisible(false)]
    public partial class DashboardPage : ContentPage
    {
        public DashboardPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<DashboardViewModel>();
        }
    }
}