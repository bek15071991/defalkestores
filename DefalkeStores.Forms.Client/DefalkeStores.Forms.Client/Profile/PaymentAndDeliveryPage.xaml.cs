﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Profile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentAndDeliveryPage : ContentPage
    {  
        public PaymentAndDeliveryPage()
        {
            InitializeComponent(); 
        }

        private async void OnBack(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync(true);
        }
    }
}