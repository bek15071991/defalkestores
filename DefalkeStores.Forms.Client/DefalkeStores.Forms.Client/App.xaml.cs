﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Forms.Client.Cart;
using DefalkeStores.Forms.Client.MyOrders;
using DefalkeStores.Forms.Client.Products;
using DefalkeStores.Forms.Client.Profile;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

[assembly: ExportFont("NunitoSans-Regular.ttf", Alias = "ThemeFontRegular")]
[assembly: ExportFont("NunitoSans-SemiBold.ttf", Alias = "ThemeFontMedium")]
[assembly: ExportFont("NunitoSans-Bold.ttf", Alias = "ThemeFontBold")]

namespace DefalkeStores.Forms.Client
{
    public partial class App : Application
    {
        public static IContainer Container;

        public App()
        {
            InitializeComponent();

            HandlingException();

            var builder = new ContainerBuilder();
            RegisterAll(builder);

            Container = builder.Build();

            DependencyResolver.ResolveUsing(type => Container.IsRegistered(type) ? Container.Resolve(type) : null);

            MainPage = new AppShell(); // new SharedTransitionNavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private static void RegisterAll(ContainerBuilder builder)
        {
            builder.RegisterModule<ApiClientModule>();

            builder.RegisterType<MessageBoxService>().As<IMessageBoxService>();

            builder.RegisterGeneric(typeof(UserService<>)).As(typeof(IUserService<>));

            builder.RegisterType<CartShoppingStorage>().As(typeof(ICartShoppingStorage));


            //builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
            //    .Where(t => t.Name.EndsWith("ViewModel"))
            //    .AsImplementedInterfaces();

            builder.RegisterType<MainViewModel>();
            builder.RegisterType<ProductDetailsViewModel>();
            builder.RegisterType<CartShoppingViewModel>().SingleInstance();
            builder.RegisterType<MakeOrderViewModel>();
            builder.RegisterType<MyOrdersViewModel>();
            builder.RegisterType<MyOrderDetailsViewModel>();
            builder.RegisterType<UserProfileViewModel>();
            builder.RegisterType<EditProfileViewModel>();
            builder.RegisterType<ProductsViewModel>();

            const string url = "http://176.123.246.252:5005"; //"http://192.168.1.109"; // 

            builder.RegisterInstance(new ClientConfiguration {Url = url});
        }

        private void HandlingException()
        {
            AppDomain.CurrentDomain.UnhandledException +=
                (sender, args) => ShowException((Exception) args.ExceptionObject);
            TaskScheduler.UnobservedTaskException += (sender, args) => ShowException(args.Exception);
        }

        private void ShowException(Exception ex)
        {
            var messageService = Container.Resolve<IMessageBoxService>();

            messageService.Notify(ex.Message, "Системная ошибка");
        }
    }
}