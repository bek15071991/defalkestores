﻿using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DefalkeStores.Application.Configurations
{
   public class CategoryConfig : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        { 
            builder.HasMany(f => f.Children)
                .WithOne(f => f.Parent).IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey("ParentId");
        }
    }
}
