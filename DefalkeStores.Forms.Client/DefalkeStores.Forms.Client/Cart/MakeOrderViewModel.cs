﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Cart
{
    public class MakeOrderViewModel : ViewModel
    {
        private string _destinationAddress;

        public string DestinationAddress
        {
            get => _destinationAddress;
            set => SetProperty(ref _destinationAddress, value);
        }

        private string _destinationName;

        public string DestinationName
        {
            get => _destinationName;
            set => SetProperty(ref _destinationName, value);
        }

        private string _destinationPhone;

        public string DestinationPhone
        {
            get => _destinationPhone;
            set => SetProperty(ref _destinationPhone, value);
        }

        private bool _includePostcard;

        public bool IncludePostcard
        {
            get => _includePostcard;
            set => SetProperty(ref _includePostcard, value, onChanged: () =>
            {
                if (Total < 0) return;

                if (value)
                    Total += _courierSettings.PostcardCost;
                else
                    Total -= _courierSettings.PostcardCost;
            });
        }


        private string _postcardText;

        public string PostcardText
        {
            get => _postcardText;
            set => SetProperty(ref _postcardText, value);
        }

        private string _senderName;

        public string SenderName
        {
            get => _senderName;
            set => SetProperty(ref _senderName, value);
        }

        private string _senderPhone;

        public string SenderPhone
        {
            get => _senderPhone;
            set => SetProperty(ref _senderPhone, value);
        }

        private bool _senderIsNewUser;

        public bool SenderIsNewUser
        {
            get => _senderIsNewUser;
            set => SetProperty(ref _senderIsNewUser, value);
        }

        private int _courierCost;

        public int CourierCost
        {
            get => _courierCost;
            set => SetProperty(ref _courierCost, value);
        }

        private int _amount;

        public int Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private int _total;

        public int Total
        {
            get => _total;
            set => SetProperty(ref _total, value);
        }

        public ICommand SendOrderCommand { get; }

        private OrderDto _orderDto;
        private CourierSettingsDto _courierSettings;

        private readonly IOrderClient _orderClient;
        private readonly IUserService<UserDto> _userService;
        private readonly ICourierClient _courierClient;
        private readonly IMessageBoxService _messageBoxService;

        public MakeOrderViewModel(IOrderClient orderClient,
            IUserService<UserDto> userService,
            ICourierClient courierClient,
            IMessageBoxService messageBoxService)
        {
            _orderClient = orderClient ?? throw new ArgumentNullException(nameof(orderClient));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _courierClient = courierClient ?? throw new ArgumentNullException(nameof(courierClient));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            SendOrderCommand = new Command(SendOrder, CanExecute);
        }

        public async Task Load(OrderDto orderDto, int amount)
        {
            _orderDto = orderDto ?? throw new ArgumentNullException(nameof(orderDto));

            var currentUser = await _userService.GetCurrentUser();
            SenderIsNewUser = currentUser == null;

            currentUser ??= orderDto.User;

            _orderDto.User = currentUser;
            SenderName = currentUser?.Name;
            SenderPhone = currentUser?.Phone;

            _courierSettings = await _courierClient.GetSettings();
            CourierCost = _courierSettings.Cost;

            Amount = amount;
            Total = amount + CourierCost;
        }

        private bool CanExecute()
        {
            return !IsBusy
                   && !string.IsNullOrEmpty(DestinationAddress)
                   && !string.IsNullOrEmpty(DestinationName)
                   && !string.IsNullOrEmpty(DestinationPhone)
                   && !string.IsNullOrEmpty(SenderName)
                   && !string.IsNullOrEmpty(SenderPhone);
        }

        private async void SendOrder()
        {
            if (SenderIsNewUser)
                _orderDto.User = new UserDto
                {
                    Name = SenderName,
                    Phone = SenderPhone
                };

            _orderDto.DestinationAddress = DestinationAddress;
            _orderDto.DestinationName = DestinationName;
            _orderDto.DestinationPhone = DestinationPhone;
            _orderDto.Total = Total;

            if (IncludePostcard)
            {
                _orderDto.Postcard = _courierSettings.PostcardCost;
                _orderDto.PostcardText = IncludePostcard ? PostcardText : null;
            }

            try
            {
                IsBusy = true;
                var savedOrder = await _orderClient.MakeOrder(_orderDto);

                await _messageBoxService.Notify("Заказ", "Заказ отправлен...");

                MessagingCenter.Send(this, MessageCenterActions.OrderCompleted);

                var user = savedOrder.User;

                await _userService.SetCurrentUser(user);
            }
            catch (Exception e)
            {  
                await _messageBoxService.Notify("Ошибка", "Возникла ошибка при заказе!");
            }
            finally
            {
                IsBusy = true;
            }
        }
    }
}