﻿using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Client.Profile;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Cart
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MakeOrderPage : ContentPage
    {
        public MakeOrderPage()
        {
            InitializeComponent();
            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<MakeOrderViewModel>();

            MessagingCenter.Subscribe<MakeOrderViewModel>(this, MessageCenterActions.OrderCompleted, async (v) =>
            { 
                await Shell.Current.GoToAsync("//Profile");

                await Shell.Current.Navigation.PushModalAsync(new PaymentAndDeliveryPage());
            });
        }

        public MakeOrderPage(OrderDto order, int amount) : this()
        {
            var viewModel = this.GetVieModel<MakeOrderViewModel>();

            Device.BeginInvokeOnMainThread(async () => { await viewModel.Load(order, amount); });
        }
    }
}