﻿using System;
using Commons.Forms;
using DefalkeStores.Api.Dto;

namespace DefalkeStores.Forms.Orders
{
    public class OrderView : ObservableObject
    {
        public int Id => Order.Id.Value;
        public string Number => Order.Number;
        public DateTime OnDate => Order.OnDate.Value;
        public int Total => Order.Total.Value;

        private OrderStatus _status;

        public OrderStatus Status
        {
            get => _status;
            set => SetProperty(ref _status, value, onChanged: () => { Order.Status = (int) value; });
        }

        public OrderDto Order { get; }

        public OrderView(OrderDto order)
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));

            Status = (OrderStatus) order.Status.Value;
        }
    }
}