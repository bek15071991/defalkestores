﻿using System.IO;
using System.Threading.Tasks;
using DefalkeStores.Forms.Products;

namespace DefalkeStores.Forms
{
    public interface IPhotoPickerService
    {
        Task<ProductImage> GetImageAsync();
    }
}