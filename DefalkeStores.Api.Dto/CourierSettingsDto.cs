﻿namespace DefalkeStores.Api.Dto
{
    public class CourierSettingsDto
    {
        public int Cost { get; set; }
        public int PostcardCost { get; set; }
    }
}