﻿using System;
using System.Drawing;
using System.IO;
using ImageMagick;

namespace Commons
{
    public static class ImageHelper
    { 
        public static byte[] MakeLightImage(byte[] image)
        {
            return Resize(image, 1024);
        }

        public static byte[] MakeThumbnail(byte[] image)
        {
            return Resize(image, 256);
        }

        private static byte[] Resize(byte[] image, int size)
        { 
            int height;
            int width;

            using (var bitmap = new Bitmap(new MemoryStream(image)))
            { 
                height = bitmap.Height;
                width = bitmap.Width; 
            } 

            if (height > size || width > size)
            {
                var max = height < width ? width : height;
                 
                var times =  (double)max / size;
                 
                height = Convert.ToInt32(height / times);
                width = Convert.ToInt32(width / times);
            }

            using (var magick = new MagickImage())
            {
                magick.Read(new MemoryStream(image));
                magick.Resize(width, height);
                magick.Strip();
                 
                using (var result = new MemoryStream())
                {
                    magick.Write(result); 
                    return result.ToArray();
                }
            } 
        }
    }
}