﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(StoresContext storesContext) : base(storesContext)
        {
        }

        public async Task<IList<Order>> Get(int count, int skip, int? userId = null, OrderStatus? orderStatus = null)
        {
            var orders = StoresContext.Orders.Include(f => f.User).AsQueryable();

            if (orderStatus != null)
                orders = orders.Where(f => f.Status == orderStatus.Value);

            if (userId != null)
                orders = orders.Where(f => f.UserId == userId.Value);

            return await orders.OrderBy(f => f.Status)
                .ThenByDescending(f => f.Id)
                .Skip(skip)
                .Take(10)
                .ToListAsync();
        }

        public async Task<IList<Order>> GetActive(int count, int skip)
        {
            var orders = StoresContext.Orders
                .Where(f => f.Status != OrderStatus.Canceled && f.Status != OrderStatus.Delivered)
                .Include(f => f.User).AsQueryable();

            return await orders.OrderByDescending(o => o.Id)
                .Skip(skip)
                .Take(10)
                .ToListAsync();
        }

        public async Task<Order> Find(int id)
        {
            return await StoresContext.Orders.FindAsync(id);
        }
    }
}