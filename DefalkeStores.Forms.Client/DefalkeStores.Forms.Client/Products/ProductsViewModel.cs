﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Products
{
    public class ProductsViewModel : ViewModel
    {
        private bool _loadingProducts;

        public bool LoadingProducts
        {
            get => _loadingProducts;
            set => SetProperty(ref _loadingProducts, value);
        }

        public CategoryDto Category { get; set; }

        private string _searchText;

        public string SearchText
        {
            get => _searchText;
            set => SetProperty(ref _searchText, value,
                onChanged: async () => await LoadProductsAsync(Category, value));
        }

        private bool _showCategories;

        public bool ShowCategories
        {
            get => _showCategories;
            set => SetProperty(ref _showCategories, value);
        }

        public ObservableCollection<CategoryDto> Categories { get; } = new ObservableCollection<CategoryDto>();
        public ObservableCollection<ProductDto> Products { get; } = new ObservableCollection<ProductDto>();

        public ICommand LoadCommand { get; }

        private readonly ICategoryClient _categoryClient;
        private readonly IProductClient _productClient;

        private int _skip;

        public ProductsViewModel(ICategoryClient categoryClient, IProductClient productClient)
        {
            _categoryClient = categoryClient;
            _productClient = productClient;

            LoadCommand = new Command<CategoryDto>(Load);
        }

        private async void Load(CategoryDto category)
        {
            Category = category ?? Category;

            Title = Category.Name; 

            await LoadCategories();

            await LoadProductsAsync(Category, SearchText);
        }

        private async Task LoadCategories()
        {
            if (Categories.Any())
                return;

            try
            {
                IsBusy = true;

                var categories = await _categoryClient.GetChildren(Category.Id);

                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowCategories = categories.Any();
                    foreach (var categoryDto in categories) Categories.Add(categoryDto);
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task LoadProductsAsync(CategoryDto category, string? productName = null)
        {
            if (category == null)
                return;

            const int count = 5;
            _skip = 0; 

            try
            {
                LoadingProducts = true;

                var products = await _productClient.Get(category: category.Id, name: productName, count: count);

                Device.BeginInvokeOnMainThread(() =>
                {
                    Products.Clear();

                    foreach (var productDto in products) Products.Add(productDto);
                });
            }
            finally
            {
                LoadingProducts = false;
            }
        }

        public async Task PartialLoadProducts(ProductDto product)
        {
            var index = Products.IndexOf(product);

            if (Products.Count - 3 != index)
                return;

            const int count = 5;
            var category = Category.Id;

            _skip += count;

            var products = await _productClient.Get(SearchText, count: count, category: category, skip: _skip);

            Device.BeginInvokeOnMainThread(() =>
            {
                foreach (var productDto in products) Products.Add(productDto);
            });
        }
    }
}