﻿using System;
using Autofac;
using Commons.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Cart
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartShoppingPage : ContentPage
    {
        public CartShoppingPage()
        {
            InitializeComponent();
            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<CartShoppingViewModel>();
            viewModel.LoadCommand.Execute(null);

            BindingContext = viewModel;
        }

        private void OnMakeOrder(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<CartShoppingViewModel>();

            var order = viewModel.MakeOrder();

            Navigation.PushModalAsync(new MakeOrderPage(order, viewModel.Amount), true);
        }
    }
}