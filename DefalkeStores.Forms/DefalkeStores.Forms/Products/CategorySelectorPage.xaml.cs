﻿using System;
using Autofac;
using DefalkeStores.Api.Dto;
using Xamarin.Forms.Xaml;
using ItemTappedEventArgs = Syncfusion.XForms.TreeView.ItemTappedEventArgs;

namespace DefalkeStores.Forms.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategorySelectorPage
    {  
        public event EventHandler<CategoryDto> CategoryChangedEvent; 

        public CategorySelectorPage(CategoryDto? category) 
        {  
              
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<CategorySelectorViewModel>();
            BindingContext = viewModel;

            viewModel.LoadCommand.Execute(category);
        } 

        private void OnCategorySelected(object sender, ItemTappedEventArgs e)
        {
            if(!(e.Node.Content is CategorySelectorViewModel.CategoryView categoryView))
                return;

            CategoryChangedEvent?.Invoke(this, categoryView.Category);
        }

        private async void OnClicked(object sender, EventArgs e)
        {  
            await Navigation.PopAsync(true);
        }
    }
}