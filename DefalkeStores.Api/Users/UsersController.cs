﻿using System;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;
using Microsoft.AspNetCore.Mvc;

namespace DefalkeStores.Api.Users
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> Edit(int userId, [FromBody]UserDto userDto)
        {
            if (userDto == null)
                return BadRequest();

            var user = await _userRepository.Find(userId);

            var isValid = UserGuard.Validate(userDto);

            if (user == null || !isValid)
                return BadRequest();

            user.Update(userDto.Name, userDto.Phone);

            await _userRepository.Save(user);

            return Ok(userDto);
        }
    }
}