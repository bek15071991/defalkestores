﻿using Autofac;
using Commons.DataAccess;
using DefalkeStores.Core;

namespace DefalkeStores.Application
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();
            builder.RegisterType<OrderRepository>().As<IOrderRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<CourierSettingsProvider>().As<ICourierSettingsProvider>();
            builder.RegisterType<BannerRepository>().As<IBannerRepository>();
             
            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>();
        }
    }
}