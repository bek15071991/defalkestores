﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Orders
{
    public class OrdersViewModel : ViewModel
    {
        private int _skip;
        public ObservableCollection<OrderView> Orders { get; set; } = new ObservableCollection<OrderView>();

        public ICommand LoadCommand { get; }
        public ICommand LoadPartitionCommand { get; }

        private readonly IOrderClient _orderClient;
        private readonly IMessageBoxService _messageBoxService;

        public OrdersViewModel(IOrderClient orderClient, IMessageBoxService messageBoxService)
        {
            _orderClient = orderClient ?? throw new ArgumentNullException(nameof(orderClient));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            LoadCommand = new Command(async () => await Load());
            LoadPartitionCommand = new Command(async () => await LoadPartition());


            MessagingCenter.Subscribe<OrderDetailsViewModel, int>(this, MessageCenterActions.ChangedOrderStatus,
                (view, id) =>
                {
                    var order = Orders.First(f => f.Id == id);

                    Orders.Remove(order);

                    order.Status = view.SelectedStatus;

                    Orders.Add(order);
                });
        }

        private async Task Load()
        {
            try
            {
                IsBusy = true;

                var activeOrders = await _orderClient.Get();

                Device.BeginInvokeOnMainThread(() =>
                {
                    Orders.Clear();

                    foreach (var activeOrder in activeOrders)
                    {
                        Orders.Add(new OrderView(activeOrder));
                    }
                });
            }
            catch (Exception)
            {
                await _messageBoxService.Notify("Произошла ошибка при загрузке");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task LoadPartition()
        {
            const int count = 5;
            _skip += count;

            try
            {
                var activeOrders = await _orderClient.Get(skip: _skip, count: count);

                foreach (var activeOrder in activeOrders)
                {
                    Orders.Add(new OrderView(activeOrder));
                }
            }
            catch (Exception)
            {
                await _messageBoxService.Notify("Ошибка при подгрузке данных");
            }
        }
    }
}