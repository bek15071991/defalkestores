﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(StoresContext storesContext) : base(storesContext)
        {
        }

        public async Task<IList<Product>> Get(int? category, string? name, int skip = 0, int count = 10)
        {
            var products = StoresContext.Products.AsQueryable();

            name = name?.ToLower();
            if (name != null)
                products = products.Where(p => p.Name.ToLower().Contains(name));

            if (category != null)
            {
                var sql = $@"
WITH c3 AS
(
  SELECT 
    id, name, parentId
  FROM categories
  WHERE  id = {category.Value}
  UNION ALL
  SELECT 
     c.id, c.name, c.parentId
  FROM categories c
  INNER JOIN c3 c1
    ON c1.Id = c.parentId
) 
SELECT 
  *
FROM c3 ;";
                 
                var categories = StoresContext.Categories.FromSqlRaw(sql)
                    .AsEnumerable()
                    .Select(f => f.Id); 

                products = products.Where(p => categories.Contains(p.CategoryId));
            }

            products = products.Where(f => f.Count > 0);

            return await products.OrderByDescending(p => p.Id).Skip(skip).Take(count).ToListAsync();
        }

        public async Task<Product?> Find(int id, bool includeImage = false)
        {
            if (includeImage)
                return await StoresContext.Products
                    .Include(f => f.OriginalImage)
                    .FirstOrDefaultAsync(f => f.Id == id);

            return await StoresContext.Products.FindAsync(id);
        }
    }
}