﻿using System;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public class Banner : IEntity
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; }

        public string Text { get; protected set; }

        public byte[] Image1 { get; protected set; }
        public byte[] Image2 { get; protected set; }
        public byte[] Image3 { get; protected set; }

        protected Banner()
        {
        }

        public Banner(int id, string name, string text, byte[] image1, byte[] image2, byte[] image3)
        {
            Id = id;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Text = text ?? throw new ArgumentNullException(nameof(text));
            Image1 = image1;
            Image2 = image2;
            Image3 = image3;
        }

        public void Set(string text, byte[] image1, byte[] image2, byte[] image3)
        {
            Text = text ?? throw new ArgumentNullException(nameof(text));
            Image1 = image1;
            Image2 = image2;
            Image3 = image3;
        }
    }
}