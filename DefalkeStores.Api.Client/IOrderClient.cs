﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using Newtonsoft.Json;

namespace DefalkeStores.Api.Client
{
    public interface IOrderClient
    {
        Task<OrderDto> MakeOrder(OrderDto order);

        Task<IList<OrderDto>> Get(OrderStatus? orderStatus = null, int? userId = null, int count = 10, int skip = 0);

        Task<IList<OrderDto>> GetActive(int count = 10, int skip = 0);

        Task UpdateStatus(int id, OrderStatus selectedStatus);
    }

    public class OrderClient : IOrderClient
    {
        private const string Path = "api/orders";
        private readonly HttpClient _httpClient;

        public OrderClient(ClientConfiguration configuration)
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.Url)
            };
        }

        public async Task<OrderDto> MakeOrder(OrderDto order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            var content = JsonConvert.SerializeObject(order);

            var result =
                await _httpClient.PostAsync(Path, new StringContent(content, Encoding.UTF8, "application/json"));

            result.EnsureSuccessStatusCode();

            var responseContent = await result.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<OrderDto>(responseContent);
        }

        public async Task<IList<OrderDto>> Get(OrderStatus? orderStatus = null,
            int? userId = null,
            int count = 10,
            int skip = 0)
        {
            var path = $"{Path}?count={count}&skip={skip}";

            if (userId != null)
                path += $"&userId={userId}";

            if (orderStatus != null)
                path += $"&orderStatus={(int)orderStatus}";

            var result = await _httpClient.GetAsync(path);

            result.EnsureSuccessStatusCode();

            var responseContent = await result.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<OrderDto>>(responseContent);
        }

        public async Task<IList<OrderDto>> GetActive(int count = 10, int skip = 0)
        {
            var path = $"{Path}/active?count={count}&skip={skip}"; 

            var result = await _httpClient.GetAsync(path);

            result.EnsureSuccessStatusCode();

            var responseContent = await result.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<OrderDto>>(responseContent);
        }

        public async Task UpdateStatus(int id, OrderStatus selectedStatus)
        {  
            var content = JsonConvert.SerializeObject(new
            {
               Status = (int)selectedStatus
            });

            var url = $"{Path}/{id}";

            var result =
                await _httpClient.PutAsync(url, new StringContent(content, Encoding.UTF8, "application/json"));

            result.EnsureSuccessStatusCode(); 
        }
    }
}