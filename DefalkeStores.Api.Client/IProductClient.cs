﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using Newtonsoft.Json;

namespace DefalkeStores.Api.Client
{
    public interface IProductClient
    {
        Task<IList<ProductDto>> Get(string? name = null, int? category = null, int count = 10, int skip = 0);

        Task Create(ProductDto dto);

        Task Update(int id, ProductDto dto);

        Task Delete(int id);

        Task<byte[]> GetImage(int id);
    }

    public class ProductClient : IProductClient
    {
        private readonly string _path;
        private readonly HttpClient _httpClient;
        private CancellationTokenSource _cancellationTokenSource;

        public ProductClient(ClientConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _path = "api/products";

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.Url)
            };
        }

        public async Task<IList<ProductDto>> Get(string? name = null, int? category = null, int count = 10, int skip = 0)
        {
            var url = $"{_path}?count={count}";

            if (name != null)
                url += $"&name={name}";

            if (category.HasValue)
                url += $"&category={category.Value}";

            url += $"&skip={skip}";

            _cancellationTokenSource?.Cancel();

            _cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(30));

            string result = null;

            try
            {
                var response = await _httpClient.GetAsync(url).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsStringAsync();
            }
            catch (TaskCanceledException)
            {
            }
            catch (Exception e)
            {
                throw e;
            }

            if (result == null)
                return null;

            return JsonConvert.DeserializeObject<IList<ProductDto>>(result);
        }

        public async Task Create(ProductDto dto)
        {
            var content = JsonConvert.SerializeObject(dto);

            var result =
                await _httpClient.PostAsync(_path, new StringContent(content, Encoding.UTF8, "application/json"))
                    .ConfigureAwait(false);

            result.EnsureSuccessStatusCode();
        }

        public async Task Update(int id, ProductDto dto)
        {
            var content = JsonConvert.SerializeObject(dto);

            var url = $"{_path}/{id}";

            var result =
                await _httpClient.PutAsync(url, new StringContent(content, Encoding.UTF8, "application/json"))
                    .ConfigureAwait(false);

            result.EnsureSuccessStatusCode();
        }

        public async Task Delete(int id)
        {
            var result = await _httpClient.DeleteAsync($"{_path}/{id}").ConfigureAwait(false);

            result.EnsureSuccessStatusCode();
        }

        public async Task<byte[]> GetImage(int id)
        {
            var url = $"{_path}/{id}/image";

            var response = await _httpClient.GetAsync(url).ConfigureAwait(false);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsByteArrayAsync();
        }
    }
}