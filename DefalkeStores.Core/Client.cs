﻿using System;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public class Client : IEntity
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; }

        public string Phone { get; protected set; }

        public string Address { get; protected set; }

        public Client(string name, string phone, string address)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
            Address = address ?? throw new ArgumentNullException(nameof(address));
        }
    }
}