﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Client.Cart;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Products
{
    public class ProductDetailsViewModel : ViewModel
    {
        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private int _price;

        public int Price
        {
            get => _price;
            set => SetProperty(ref _price, value);
        }

        private int _count;

        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }

        private string _description;

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        private ObservableCollection<byte[]> _images;

        public ObservableCollection<byte[]> Images
        {
            get => _images;
            set => SetProperty(ref _images, value);
        }

        public ICommand AddToCartCommand => new Command(AddToCart);

        private ProductDto _productDto;
        private readonly IMessageBoxService _messageBoxService;
        private readonly IProductClient _productClient;
        private readonly ICartShoppingStorage _cartShoppingStorage;

        public ProductDetailsViewModel(IMessageBoxService messageBoxService,
            IProductClient productClient,
            ICartShoppingStorage cartShoppingStorage)
        {
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            _productClient = productClient ?? throw new ArgumentNullException(nameof(productClient));
            _cartShoppingStorage = cartShoppingStorage ?? throw new ArgumentNullException(nameof(cartShoppingStorage));
        }

        public void Load(ProductDto product)
        {
            _productDto = product ??
                          throw new ArgumentNullException(nameof(product));

            Name = product.Name;
            Price = product.Price;
            Description = product.Description;
            Count = product.Count;
            Images = new ObservableCollection<byte[]>{ product.Image };

            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    IsBusy = true;
                    var image = await _productClient.GetImage(product.Id);
                    Images = new ObservableCollection<byte[]> {image };
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
                finally
                {
                    IsBusy = false;
                }
            });
        }

        private async void AddToCart()
        {
            if (_productDto == null)
                throw new ArgumentNullException(nameof(_productDto));

            var shoppingViewModel = App.Container.Resolve<CartShoppingViewModel>();
            await shoppingViewModel.Add(_productDto);

            await _messageBoxService.Notify($"'{Name}' добавлен в корзину.", "Корзина");
        }
    }
}