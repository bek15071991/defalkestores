﻿using System.Threading.Tasks;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public interface IBannerRepository : ISaveRepository<Banner>
    {
        Task<Banner> GetMain();
    }
}