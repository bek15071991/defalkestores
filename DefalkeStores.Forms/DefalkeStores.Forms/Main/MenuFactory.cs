﻿using System.Collections.Generic;

namespace DefalkeStores.Forms.Main
{
    public class MenuFactory
    {
        public static IList<HomeMenuItem> Create()
        {
            return new List<HomeMenuItem>
            { 
                new HomeMenuItem {Id = MenuItemType.Dashboard, Title = "Главный"},
                new HomeMenuItem {Id = MenuItemType.Categories, Title = "Категории"},
                new HomeMenuItem {Id = MenuItemType.Products, Title = "Товары"},
                new HomeMenuItem {Id = MenuItemType.Orders, Title = "Заказы"},
                new HomeMenuItem {Id = MenuItemType.Banners, Title = "Реклама"},
            };
        }
    }
}