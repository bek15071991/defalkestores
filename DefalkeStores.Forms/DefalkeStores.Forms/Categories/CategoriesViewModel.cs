﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Categories
{
    public class CategoriesViewModel : ViewModel
    {
        public ObservableCollection<CategoryDto> Categories { get; } = new ObservableCollection<CategoryDto>();

        public ICommand LoadCategoriesCommand { get; } 

        private readonly ICategoryClient _categoryClient; 

        public CategoriesViewModel(ICategoryClient categoryClient)
        {
            _categoryClient = categoryClient ?? throw new ArgumentNullException(nameof(categoryClient)); 
            Title = "Категории";

            LoadCategoriesCommand = new Command(LoadCategories); 
        } 

        private async void LoadCategories()
        {
            IsBusy = true;

            try
            {
                Categories.Clear();

                var categories = await _categoryClient.GetRoot();

                foreach (var categoryDto in categories)
                {
                    Categories.Add(categoryDto);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            IsBusy = false;
        }
    }
}