﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public interface IProductRepository : IDeleteRepository<Product>, ISaveRepository<Product>
    {
        Task<IList<Product>> Get(int? category, string? name, int skip = 0, int count = 10);

        Task<Product?> Find(int id, bool includeImage = false); 
    }
}