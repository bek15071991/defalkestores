﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using Newtonsoft.Json;

namespace DefalkeStores.Api.Client
{
    public interface IUserClient
    {
        Task<UserDto> Update(UserDto userDto);
    }

    public class UserClient : IUserClient
    {
        private readonly string _path;
        private readonly HttpClient _httpClient;

        public UserClient(ClientConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.Url)
            };

            _path = "api/users";
        }

        public async Task<UserDto> Update(UserDto userDto)
        {
            if (userDto == null)
                throw new ArgumentNullException(nameof(userDto));

            var content = JsonConvert.SerializeObject(userDto);

            var result = await _httpClient.PutAsync($"{_path}/{userDto.Id}",
                new StringContent(content, Encoding.UTF8, 
                "application/json"));

            result.EnsureSuccessStatusCode();

            var userResult = await result.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<UserDto>(userResult);
        }
    }
}