﻿using Autofac;

namespace DefalkeStores.Api.Client
{
    public class ApiClientModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CategoryClient>().As<ICategoryClient>().SingleInstance();
            builder.RegisterType<ProductClient>().As<IProductClient>().SingleInstance();
            builder.RegisterType<OrderClient>().As<IOrderClient>().SingleInstance();
            builder.RegisterType<CourierClient>().As<ICourierClient>().SingleInstance();
            builder.RegisterType<UserClient>().As<IUserClient>().SingleInstance();
            builder.RegisterType<BannerClient>().As<IBannerClient>().SingleInstance();
        }
    }
}