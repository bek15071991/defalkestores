﻿using System;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsPage : ContentPage
    {
        public ProductsPage(CategoryDto category, string searchText)
        {
            if (category == null) 
                throw new ArgumentNullException(nameof(category)); 

            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<ProductsViewModel>();

            BindingContext = viewModel;

            viewModel.SearchText = searchText;
            viewModel.LoadCommand.Execute(category);
        }

        private void OnSearch(object sender, EventArgs e)
        {
            if (!(sender is Entry searchEntry))
                return;

            var viewModel = (ProductsViewModel)BindingContext;
            viewModel.SearchText = searchEntry.Text;
        }

        private async void OnCategorySelected(object sender, EventArgs e)
        {
            if (!(e is TappedEventArgs args))
                return; 

            UpdateCategorySelect((View)sender);

            var viewModel = this.GetVieModel<ProductsViewModel>();

            var category = (CategoryDto)args.Parameter;  
            await Shell.Current.Navigation.PushAsync(new ProductsPage(category, viewModel.SearchText));
        }

        private async void OnEndingList(object sender, ItemVisibilityEventArgs e)
        {
            if (!(e.Item is ProductDto product)) return;

            var viewModel = (ProductsViewModel)BindingContext;

            await viewModel.PartialLoadProducts(product);
        }

        private async void OnProductSelected(object sender, ItemTappedEventArgs e)
        {
            if (!(e.Item is ProductDto product))
                return;

            await Shell.Current.Navigation.PushAsync(new ProductDetailsPage(product));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateCategorySelect();
        } 
        private void UpdateCategorySelect(View? view = null)
        {
            foreach (var child in categories.Children)
            {
                VisualStateManager.GoToState(child, "Normal");
                ChangeTextColor(child, "#707070");
            }

            if (view != null)
            {
                VisualStateManager.GoToState(view, "Selected");
                ChangeTextColor(view, "#FFFFFF");
            }

            void ChangeTextColor(View child, string hexColor)
            {
                var txtCtrl = child.FindByName<Label>("CategoryName");

                if (txtCtrl != null)
                    txtCtrl.TextColor = Color.FromHex(hexColor);
            }
        }
    }
}