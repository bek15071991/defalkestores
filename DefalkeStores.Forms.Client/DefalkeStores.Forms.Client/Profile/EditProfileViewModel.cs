﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Profile
{
    public class EditProfileViewModel : ViewModel
    {
        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _phone;

        public string Phone
        {
            get => _phone;
            set => SetProperty(ref _phone, value);
        }

        public ICommand SaveCommand { get; }
        public ICommand LoadCommand { get; } 

        private readonly IUserClient _userClient;
        private readonly IMessageBoxService _messageBoxService;
        private readonly IUserService<UserDto> _userService;

        private UserDto _userDto;

        public EditProfileViewModel(IUserClient userClient,
            IMessageBoxService messageBoxService,
            IUserService<UserDto> userService)
        {
            _userClient = userClient ?? throw new ArgumentNullException(nameof(userClient));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));

            SaveCommand = new Command(async () => await Save(),
                () => !string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Phone));

            LoadCommand = new Command(async () => await Load()); 
        }

        private async Task Load()
        {
            _userDto = await _userService.GetCurrentUser();
            Name = _userDto.Name;
            Phone = _userDto.Phone;
        }

        private async Task Save()
        {
            _userDto.Name = Name;
            _userDto.Phone = Phone;

            try
            {
                IsBusy = true;

                await _userClient.Update(_userDto);

                await _userService.SetCurrentUser(_userDto);

                await _messageBoxService.Notify("Сохранено");
            }
            catch (Exception e)
            {
                await _messageBoxService.Notify("Произошла ошибка при сохранении");
            }
            finally
            {
                IsBusy = false;
            } 
        }
    }
}