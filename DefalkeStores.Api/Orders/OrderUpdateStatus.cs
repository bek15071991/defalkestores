﻿using DefalkeStores.Api.Dto;

namespace DefalkeStores.Api.Orders
{
    public class OrderUpdateStatus
    {
        public OrderStatus Status { get; set; }
    }
}
