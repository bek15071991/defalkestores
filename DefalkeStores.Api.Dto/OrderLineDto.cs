﻿using System.Collections.Generic;

namespace DefalkeStores.Api.Dto
{
    public class OrderLineDto
    {
        public int ProductId { get; set; }

        public string? ProductName { get; set; }

        public int? Price { get; set; }
        
        public int Count { get; set; }

        public List<byte>? ProductThumbnailImage { get; set; }
    }
}