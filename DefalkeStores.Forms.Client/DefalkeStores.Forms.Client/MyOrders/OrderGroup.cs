﻿using System;
using System.Collections.Generic;
using DefalkeStores.Api.Dto;

namespace DefalkeStores.Forms.Client.MyOrders
{
    public class OrderGroup : List<OrderDto>
    {
        public DateTime OnDate { get; }

        public OrderGroup(DateTime onDate, IList<OrderDto> orders) : base(orders)
        {
            OnDate = onDate;
        }
    }
}