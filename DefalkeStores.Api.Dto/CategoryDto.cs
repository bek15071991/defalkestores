﻿namespace DefalkeStores.Api.Dto
{
    public class CategoryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? Parent { get; set; }
    }
}