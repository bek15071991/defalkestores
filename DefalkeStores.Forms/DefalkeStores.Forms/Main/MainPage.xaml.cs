﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using DefalkeStores.Forms.Banners;
using DefalkeStores.Forms.Categories;
using DefalkeStores.Forms.Orders;
using DefalkeStores.Forms.Products;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Main
{ 
    [DesignTimeVisible(false)]
    public partial class MainPage
    {
        private readonly Dictionary<MenuItemType, NavigationPage> _menuPages = new Dictionary<MenuItemType, NavigationPage>();
        public MainPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.SplitOnPortrait;

            _menuPages.Add(MenuItemType.Dashboard, (NavigationPage)Detail);
        }

        public async Task NavigateFromMenu(MenuItemType id)
        {
            if (!_menuPages.ContainsKey(id))
            {
                switch (id)
                {    
                    case MenuItemType.Categories:
                        _menuPages.Add(id, new NavigationPage(new CategoriesPage()));
                        break;

                    case MenuItemType.Products:
                        _menuPages.Add(id, new NavigationPage(new ProductsPage()));
                        break;

                    case MenuItemType.Orders:
                        _menuPages.Add(id, new NavigationPage(new OrdersPage()));
                        break;
                    
                    case MenuItemType.Banners:
                        _menuPages.Add(id, new NavigationPage(new BannerPage()));
                        break;
                }
            }

            var newPage = _menuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
        }
    }
}