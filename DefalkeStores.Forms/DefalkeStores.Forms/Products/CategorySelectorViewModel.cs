﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Syncfusion.TreeView.Engine;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Products
{
    public class CategorySelectorViewModel : ViewModel
    {
        private AutoExpandMode _expandMode; 
        public AutoExpandMode ExpandMode
        {
            get => _expandMode;
            set => SetProperty(ref _expandMode, value);
        } 

        private CategoryView _selectedCategory; 
        public CategoryView SelectedCategory
        {
            get => _selectedCategory;
            set => SetProperty(ref _selectedCategory, value);
        }

        public ObservableCollection<CategoryView> Categories { get; set; } = new ObservableCollection<CategoryView>();

        public ICommand LoadCommand { get; }

        private int? _categoryId;

        private readonly ICategoryClient _categoryClient;
        private readonly IMessageBoxService _messageBoxService;

        public CategorySelectorViewModel(ICategoryClient categoryClient, IMessageBoxService messageBoxService)
        {
            _categoryClient = categoryClient ?? throw new ArgumentNullException(nameof(categoryClient));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            LoadCommand = new Command<CategoryDto>(Load);
        }

        private async void Load(CategoryDto category)
        {
            _categoryId = category?.Id;
            ExpandMode = category != null
                ? AutoExpandMode.AllNodesExpanded
                : AutoExpandMode.None;

            try
            {
                IsBusy = true;

                var categories = await _categoryClient.Get(count: 10000);

                var groupedCategories = GenerateHierarchy(categories);

                Categories = new ObservableCollection<CategoryView>(groupedCategories);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                await _messageBoxService.Notify("Cannot load categories");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private List<CategoryView> GenerateHierarchy(ICollection<CategoryDto> categoryList, int? parentId = null)
        {
            var catHierarchy = new List<CategoryView>();

            foreach (var cat in categoryList.Where(c => c.Parent == parentId))
            {
                var children = GenerateHierarchy(categoryList, cat.Id);

                var view = new CategoryView(cat, children);
                catHierarchy.Add(view);

                if (_categoryId == view.Id)
                    SelectedCategory = view;
            }

            return catHierarchy;
        }

        public class CategoryView : ObservableObject
        {
            public int Id { get; set; }

            private string _name;

            public string Name
            {
                get => _name;
                set => SetProperty(ref _name, value);
            }

            public ObservableCollection<CategoryView> Children { get; }
            public CategoryDto Category { get; }

            public CategoryView(CategoryDto category, ICollection<CategoryView> children = null)
            {
                Id = category.Id;
                Name = category.Name;
                Children = children == null
                    ? new ObservableCollection<CategoryView>()
                    : new ObservableCollection<CategoryView>(children);

                Category = category;
            }
        }
    }
}