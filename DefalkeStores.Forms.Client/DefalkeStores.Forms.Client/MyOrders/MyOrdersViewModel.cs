﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.MyOrders
{
    public class MyOrdersViewModel : ViewModel
    {
        public ObservableCollection<OrderGroup> Orders { get; } = new ObservableCollection<OrderGroup>();

        public ICommand LoadOrdersCommand { get; }

        private readonly IOrderClient _orderClient;
        private readonly IUserService<UserDto> _userService;

        public MyOrdersViewModel(IOrderClient orderClient, IUserService<UserDto> userService)
        {
            _orderClient = orderClient ?? throw new ArgumentNullException(nameof(orderClient));
            LoadOrdersCommand = new Command(LoadOrders);
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        private async void LoadOrders()
        {
            try
            {
                IsBusy = true;

                var user = await _userService.GetCurrentUser();

                if (user == null)
                    return;

                var orders = await _orderClient.Get(userId: user.Id);

                var groupedOrders = orders.GroupBy(f => f.OnDate.GetValueOrDefault().Date)
                    .Select(f => new OrderGroup(f.Key, f.ToList()));

                Device.BeginInvokeOnMainThread(() =>
                {
                    Orders.Clear();

                    foreach (var order in groupedOrders)
                    {
                        Orders.Add(order);
                    }
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}