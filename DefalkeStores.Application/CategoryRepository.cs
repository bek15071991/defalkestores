﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(StoresContext storesContext) : base(storesContext)
        {
        }

        public async Task<IList<Category>> Get(string? name, int count)
        {
            var categories = StoresContext.Categories.AsQueryable();

            name = name?.ToLower();
            if (name != null)
                categories = categories.Where(c => c.Name.Contains(name));

            return await categories.OrderByDescending(c => c.Id).Take(count).ToListAsync();
        }

        public async Task<IList<Category>> GetRoot()
        {
            return await StoresContext.Categories.Where(f => f.ParentId == null).ToListAsync();
        }

        public async Task<Category> Find(int id)
        {
            return await StoresContext.Categories.FindAsync(id);
        }

        public async Task<IList<Category>> GetChildren(int id)
        {
            return await StoresContext.Categories.Where(f => f.ParentId == id)
                .OrderBy(c => c.Name)
                .ToListAsync();
        }
    }
}