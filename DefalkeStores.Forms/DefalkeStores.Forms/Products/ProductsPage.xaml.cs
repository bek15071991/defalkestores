﻿using System;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsPage : ContentPage
    {
        public ProductsPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<ProductsViewModel>();
        }

        private async void OnAddProduct(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProductPage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var viewModel = this.GetVieModel<ProductsViewModel>();

            if (viewModel.Products.Count == 0)
                viewModel.LoadProductsCommand.Execute(null);
        }

        private async void OnProductSelected(object sender, ItemTappedEventArgs e)
        {
            if (!(e.Item is ProductDto product))
                return;

            await Navigation.PushAsync(new ProductPage(product));
        }

        private async void OnEndingList(object sender, ItemVisibilityEventArgs e)
        {
            if (!(e.Item is ProductDto product)) return;

            var viewModel = (ProductsViewModel)BindingContext;

            await viewModel.PartialLoadProducts(product);
        }
    }
}