﻿using System.Windows.Input;
using Commons.Forms;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Cart
{
    public class CartLine : ObservableObject
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int ProductPrice { get; set; }

        public byte[] ProductImage { get; set; }

        public int RemainderOfProducts { get; set; } 

        private int _count;

        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }

        public ICommand IncreaseCommand => new Command<CartLine>(Increase);
        public ICommand DecreaseCommand => new Command<CartLine>(Decrease);

        private void Increase(CartLine cartLine)
        {
            if (RemainderOfProducts <= cartLine.Count)
                return;

            cartLine.Count++;
        }

        private static void Decrease(CartLine cartLine)
        {
            if (cartLine.Count == 1)
                return;

            cartLine.Count--;
        }
    }
}