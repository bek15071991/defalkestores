#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
# install System.Drawing native dependencies RUN ON SERVER
RUN apt-get update \
    && apt-get install -y --allow-unauthenticated \
        libc6-dev \
        libgdiplus \
        libx11-dev \
     && rm -rf /var/lib/apt/lists/*

WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["DefalkeStores.Api/DefalkeStores.Api.csproj", "DefalkeStores.Api/"]
COPY ["DefalkeStores.Application/DefalkeStores.Application.csproj", "DefalkeStores.Application/"]
COPY ["DefalkeStores.Api.Dto/DefalkeStores.Api.Dto.csproj", "DefalkeStores.Api.Dto/"]
COPY ["DefalkeStores.Core/DefalkeStores.Core.csproj", "DefalkeStores.Core/"]
COPY ["Commons/Commons.csproj", "Commons/"]
COPY ["Commons.DataAccess/Commons.DataAccess.csproj", "Commons.DataAccess/"]
RUN dotnet restore "DefalkeStores.Api/DefalkeStores.Api.csproj"
COPY . .
WORKDIR "/src/DefalkeStores.Api"
RUN dotnet build "DefalkeStores.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DefalkeStores.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DefalkeStores.Api.dll"]