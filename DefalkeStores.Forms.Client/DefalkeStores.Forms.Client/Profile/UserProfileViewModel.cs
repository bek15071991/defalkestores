﻿using System;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client.Profile
{
    public class UserProfileViewModel : ViewModel
    {
        private bool _authorized;

        public bool Authorized
        {
            get => _authorized;
            set => SetProperty(ref _authorized, value);
        }

        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _phone;

        public string Phone
        {
            get => _phone;
            set => SetProperty(ref _phone, value);
        }

        public ICommand LoadCommand { get; }

        private readonly IUserService<UserDto> _userService;

        public UserProfileViewModel(IUserService<UserDto> userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            LoadCommand = new Command(Load);
        }

        private async void Load()
        {
            var user = await _userService.GetCurrentUser();

            Authorized = user != null;

            Name = user?.Name;
            Phone = user?.Phone;
        }
    }
}