﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Helpers; 
using Xamarin.Forms;

namespace DefalkeStores.Forms.Products
{
    public class ProductsViewModel : ViewModel
    {
        public ObservableCollection<ProductDto> Products { get; } = new ObservableCollection<ProductDto>();

        public ICommand LoadProductsCommand { get; }

        private readonly IProductClient _productClient;
        private int _skip;

        public ProductsViewModel(IProductClient productClient)
        {
            _productClient = productClient ?? throw new ArgumentNullException(nameof(productClient));
            Title = "Товары";

            LoadProductsCommand = new Command(LoadProducts);

            Subscribe();
        } 

        private void Subscribe()
        {
            MessagingCenter.Subscribe<ProductViewModel, ProductDto>(this, MessageCenterActions.AddedProduct,
                (model, newProduct) => { Products.Add(newProduct); });

            MessagingCenter.Subscribe<ProductViewModel, ProductDto>(this, MessageCenterActions.UpdatedProduct,
                (model, updatedProduct) =>
                {
                    var oldProduct = Products.FirstOrDefault(p => p.Id == updatedProduct.Id);
                    Products.Remove(oldProduct);
                    Products.Add(updatedProduct);
                });

            MessagingCenter.Subscribe<ProductViewModel, int>(this, MessageCenterActions.DeletedProduct,
                (model, id) =>
                {
                    var oldProduct = Products.FirstOrDefault(p => p.Id == id);
                    Products.Remove(oldProduct);
                });
        }

        private async void LoadProducts()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Products.Clear();

                var products = await _productClient.Get(count:20);

                foreach (var productDto in products)
                {
                    Products.Add(productDto);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }


            IsBusy = false;
        }

        public async Task PartialLoadProducts(ProductDto product)
        {
            var index = Products.IndexOf(product);

            if (Products.Count - 3 != index)
                return;

            const int count = 10; 

            _skip += count;

            var products = await _productClient.Get(count: count, skip: _skip);

            Device.BeginInvokeOnMainThread(() =>
            {
                foreach (var productDto in products) Products.Add(productDto);
            });
        }
    }
}