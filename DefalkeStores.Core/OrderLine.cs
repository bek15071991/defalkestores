﻿using System;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public class OrderLine : IEntity
    {
        public int Id { get; protected set; }

        public Product Product { get; protected set; }
        public int ProductId { get; protected set; }

        public int Count { get; protected set; }

        protected OrderLine() // for ef
        {
        }

        public OrderLine(Product product, int count)
        {
            Product = product ?? throw new ArgumentNullException(nameof(product));

            if (count < 0)
                throw new ArgumentException($"{nameof(count)} must be more then 0");
            Count = count;
            ProductId = ProductId;
            product.Decrease(count);
        }
    }
}