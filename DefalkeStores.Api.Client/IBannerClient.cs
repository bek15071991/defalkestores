﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using Newtonsoft.Json;

namespace DefalkeStores.Api.Client
{
    public interface IBannerClient
    {
        Task<BannerDto> GetMain();

        Task<BannerDto> Save(BannerDto banner);
    }

    public class BannerClient : IBannerClient
    {
        private readonly ClientConfiguration _configuration;
        private readonly string _path;
        private readonly HttpClient _httpClient;

        public BannerClient(ClientConfiguration configuration)
        {
            _configuration = configuration;

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.Url)
            };

            _path = "api/banners";
        }

        public async Task<BannerDto> GetMain()
        {
            var url = $"{_path}/main";

            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<BannerDto>(result);
        }

        public async Task<BannerDto> Save(BannerDto banner)
        {
            if (banner == null)
                throw new ArgumentNullException(nameof(banner));

            var content = JsonConvert.SerializeObject(banner);

            var result =
                await _httpClient.PutAsync($"{_path}", new StringContent(content, Encoding.UTF8, "application/json"));
            result.EnsureSuccessStatusCode();

            var resultContent = await result.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<BannerDto>(resultContent);
        }
    }
}