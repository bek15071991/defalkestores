﻿using System;
using Autofac;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.MyOrders
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyOrdersPage : ContentPage
    {
        public MyOrdersPage()
        {
            InitializeComponent();
            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<MyOrdersViewModel>();
            BindingContext = viewModel; 

            viewModel.LoadOrdersCommand.Execute(null);
        }   

        private void OnOrderSelected(object sender, ItemTappedEventArgs e)
        {  
            if (!(e.Item is OrderDto order))
                return;

            Navigation.PushModalAsync(new MyOrderDetailsPage(order));
        }
    }
}