﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Client
{
    public class MainViewModel : ViewModel
    {
        public bool ShowBannerImage { get; set; }

        public CategoryDto SelectedCategory { get; set; }

        private bool _loadingProducts;

        public bool LoadingProducts
        {
            get => _loadingProducts;
            set => SetProperty(ref _loadingProducts, value);
        }

        private string _searchText;

        public string SearchText
        {
            get => _searchText;
            set => SetProperty(ref _searchText, value,
                onChanged: async () => await LoadProductsAsync(SelectedCategory, SearchText));
        }

        private string _bannerText;

        public string BannerText
        {
            get => _bannerText;
            set => SetProperty(ref _bannerText, value);
        }

        public ObservableCollection<CategoryDto> Categories { get; } = new ObservableCollection<CategoryDto>();
        public ObservableCollection<ProductDto> Products { get; } = new ObservableCollection<ProductDto>();
        public ObservableCollection<byte[]> Banners { get; } = new ObservableCollection<byte[]>();

        public ICommand LoadCommand { get; }

        private readonly ICategoryClient _categoryClient;
        private readonly IProductClient _productClient;
        private readonly IBannerClient _bannerClient;

        private int _skip;

        public MainViewModel(ICategoryClient categoryClient,
            IProductClient productClient,
            IBannerClient bannerClient)
        {
            _categoryClient = categoryClient ?? throw new ArgumentNullException(nameof(categoryClient));
            _productClient = productClient ?? throw new ArgumentNullException(nameof(productClient));
            _bannerClient = bannerClient ?? throw new ArgumentNullException(nameof(bannerClient));

            LoadCommand = new Command(async () => Load());
        }

        private async Task Load()
        {
            await LoadBanners();
            await LoadCategories();
            await LoadProductsAsync(SelectedCategory, SearchText);
        }

        private async Task LoadBanners()
        {
            try
            {
                var mainBanner = await _bannerClient.GetMain();

                BannerText = mainBanner.Text;

                if (mainBanner.Image1 != null)
                    Banners.Add(mainBanner.Image1);

                if (mainBanner.Image2 != null)
                    Banners.Add(mainBanner.Image2);

                if (mainBanner.Image2 != null)
                    Banners.Add(mainBanner.Image3);

                ShowBannerImage = Banners.Any();
            }
            catch (Exception e)
            {
                BannerText = "Здесь может быть ваша реклама";
            }
        }

        private async Task LoadCategories()
        {
            if (Categories.Any())
                return;

            try
            {
                IsBusy = true;

                var categories = await _categoryClient.GetRoot();

                Device.BeginInvokeOnMainThread(() =>
                {
                    foreach (var categoryDto in categories) Categories.Add(categoryDto);
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task LoadProductsAsync(CategoryDto category, string? productName = null)
        {
            const int count = 5;
            _skip = 0;

            try
            {
                LoadingProducts = true;

                var products = category == null
                    ? await _productClient.Get(count: count, name: productName)
                    : await _productClient.Get(category: category.Id, name: productName, count: count);

                Device.BeginInvokeOnMainThread(() =>
                {
                    Products.Clear();

                    foreach (var productDto in products) Products.Add(productDto);
                });
            }
            finally
            {
                LoadingProducts = false;
            }
        }

        public async Task PartialLoadProducts(ProductDto product)
        {
            var index = Products.IndexOf(product);

            if (Products.Count - 3 != index)
                return;

            const int count = 5;

            _skip += count;

            var products = SelectedCategory == null
                ? await _productClient.Get(name: SearchText, count: count, skip: _skip)
                : await _productClient.Get(SearchText, count: count, category: SelectedCategory.Id, skip: _skip);

            Device.BeginInvokeOnMainThread(() =>
            {
                foreach (var productDto in products) Products.Add(productDto);
            });
        }
    }
}