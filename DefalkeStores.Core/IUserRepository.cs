﻿using System.Threading.Tasks;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public interface IUserRepository : ISaveRepository<User>
    {
        Task<User?> Find(int id0);
    }
}