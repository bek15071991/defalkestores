﻿using System; 
using System.Text;
using Microsoft.AspNetCore.Http; 
using System.Threading.Tasks;
using Serilog;

namespace DefalkeStores.Api.Infrastructure
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public RequestLoggingMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            finally
            {
                var request = context.Request;
                var body = request.Body;
                var buffer = new byte[Convert.ToInt32(request.ContentLength)];
                await request.Body.ReadAsync(buffer, 0, buffer.Length);
                var requestBody = Encoding.UTF8.GetString(buffer);
                request.Body = body;

                _logger.Information(
                    "Request {method} {url} => {statusCode}",
                    context.Request?.Method,
                    request?.Path.Value, 
                    context.Response?.StatusCode,
                    request?.ContentLength,
                    requestBody);
            }
        }  
    }
}