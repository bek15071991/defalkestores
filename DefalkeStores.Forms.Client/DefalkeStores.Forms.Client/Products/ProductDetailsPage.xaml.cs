﻿using System;
using System.Collections.Generic;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Client.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductDetailsPage : ContentPage
    {
        private double _lastScrollIndex;
        private double _currentScrollIndex;
        public ProductDetailsPage()
        {
            InitializeComponent();
            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<ProductDetailsViewModel>();
        }

        public ProductDetailsPage(ProductDto product) :this()
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            var viewModel = this.GetVieModel<ProductDetailsViewModel>();
            viewModel.Load(product);
        }

        private void OnGoBack(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        } 

        private void OnAddToCart(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<ProductDetailsViewModel>();
            viewModel.AddToCartCommand.Execute(null);
        }

        private void ScrollView_Scrolled(object sender, ScrolledEventArgs e)
        {
            _currentScrollIndex = e.ScrollY;
            footer.IsVisible = !(_currentScrollIndex > _lastScrollIndex);
            _lastScrollIndex = _currentScrollIndex;
        }

        private async void OnTapImage(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<ProductDetailsViewModel>();
             
            await Shell.Current.Navigation.PushAsync(new ImageViewerPage(viewModel.Images));
        }
    }
}