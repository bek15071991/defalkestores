﻿using System.Threading.Tasks;

namespace Commons.DataAccess
{
    public interface IDeleteRepository<T>
    {
        Task Delete(T t);
    }
}