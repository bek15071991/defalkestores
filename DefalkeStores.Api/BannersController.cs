﻿using System;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;
using Microsoft.AspNetCore.Mvc;

namespace DefalkeStores.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannersController : ControllerBase
    {
        private readonly IBannerRepository _bannerRepository;

        public BannersController(IBannerRepository bannerRepository)
        {
            _bannerRepository = bannerRepository ?? throw new ArgumentNullException(nameof(bannerRepository));
        }

        [HttpGet("main")]
        public async Task<IActionResult> Get()
        {
            var banner = await _bannerRepository.GetMain();

            return Ok(new BannerDto
            {
                Id = banner.Id,
                Name = banner.Name,
                Text = banner.Text,
                Image1 = banner.Image1,
                Image2 = banner.Image2,
                Image3 = banner.Image3
            });
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] BannerDto bannerDto)
        {
            var banner = await _bannerRepository.GetMain();

            banner.Set(bannerDto.Text, bannerDto.Image1, bannerDto.Image2, bannerDto.Image3);

            await _bannerRepository.Save(banner);

            return Ok(bannerDto);
        }
    }
}