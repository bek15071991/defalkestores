﻿using System.Threading.Tasks;

namespace Commons.DataAccess
{
    public interface ISaveRepository<T> where T : IEntity
    {
        Task<T> Save(T t);
    }
}