﻿using Autofac;
using DefalkeStores.Api.Dto;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Orders
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderDetailsPage
    {
        public OrderDetailsPage(OrderDto order)
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<OrderDetailsViewModel>();
            BindingContext = viewModel;

            viewModel.LoadCommand.Execute(order);
        }
    }
}