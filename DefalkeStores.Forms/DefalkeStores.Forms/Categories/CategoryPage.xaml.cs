﻿using System;
using Autofac;
using Commons.Forms;
using DefalkeStores.Api.Dto;
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Categories
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryPage
    {
        public CategoryPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<CategoryViewModel>();

            MessagingCenter.Subscribe<CategoryViewModel>(this, MessageCenterActions.ChangedCategory,
                async view => { await Navigation.PopAsync(true); });
        }

        public CategoryPage(CategoryDto category, string parentName = null)
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            BindingContext = scope.Resolve<CategoryViewModel>();

            var viewModel = this.GetVieModel<CategoryViewModel>();

            viewModel.Load(category, parentName);

            MessagingCenter.Subscribe<CategoryViewModel>(this, MessageCenterActions.ChangedCategory + category.Id,
                async view =>
                {
                    //  viewModel.Load(category, parentName);
                    await Navigation.PopAsync(true);
                });
        }

        private async void OnChildTapped(object sender, ItemTappedEventArgs e)
        {
            if (!(e.Item is CategoryDto category))
                return;

            var viewModel = this.GetVieModel<CategoryViewModel>();

            await Navigation.PushAsync(new CategoryPage(category, viewModel.Name));
        }

        private async void OnAddChildCategory(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<CategoryViewModel>();

            var categoryId = viewModel.Id;
            var categoryName = viewModel.Name;

            await Navigation.PushAsync(new SubCategoryPage(categoryId, categoryName));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var viewModel = this.GetVieModel<CategoryViewModel>();

            viewModel.LoadChildrenCommand.Execute(null);
        }
    }
}