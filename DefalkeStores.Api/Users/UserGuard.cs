﻿using DefalkeStores.Api.Dto;

namespace DefalkeStores.Api.Users
{
    public class UserGuard
    {
        public static bool Validate(UserDto userDto)
        {
            return userDto != null
                   && !string.IsNullOrEmpty(userDto.Name)
                   && !string.IsNullOrEmpty(userDto.Phone);
        }
    }
}
