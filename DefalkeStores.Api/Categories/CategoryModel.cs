﻿using System.ComponentModel.DataAnnotations;

namespace DefalkeStores.Api.Categories
{
    public class CategoryModel
    {
        [Required(ErrorMessage = "Name must be not empty")]
        public string Name { get; set; }

        public int? ParentId{ get; set; }
    }
}
