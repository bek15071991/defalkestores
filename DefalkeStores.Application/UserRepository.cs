﻿using System.Threading.Tasks;
using DefalkeStores.Core;

namespace DefalkeStores.Application
{
   public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(StoresContext storesContext) : base(storesContext)
        {
        }

        public async Task<User> Find(int id)
        {
            return await StoresContext.Users.FindAsync(id);
        }
    }
}