﻿using System.Drawing;
using System.IO;

namespace Commons
{
    public static class ImageConvertHelper
    {
        public static byte[] ToArray(this Stream stream)
        {
            byte[] result;
            using (var streamReader = new MemoryStream())
            {
                stream.CopyTo(streamReader);
                result = streamReader.ToArray();
            }

            return result;
        }

        public static byte[] ToArray(this Image image)
        {
            using (var stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                return stream.ToArray();
            }
        }
    }
}