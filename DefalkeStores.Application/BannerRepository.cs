﻿using System.Threading.Tasks;
using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;

namespace DefalkeStores.Application
{
    public class BannerRepository : RepositoryBase<Banner>, IBannerRepository
    {
        public BannerRepository(StoresContext storesContext) : base(storesContext)
        {
        }

        public async Task<Banner> GetMain()
        {
            return await StoresContext.Banners.FirstAsync();
        }
    }
}