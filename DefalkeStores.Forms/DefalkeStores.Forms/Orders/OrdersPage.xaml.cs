﻿using Autofac;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Orders
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrdersPage
    {
        public OrdersPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<OrdersViewModel>();
            BindingContext = viewModel;

            viewModel.LoadCommand.Execute(null);
        }

        private async void OnSelectedItem(object sender, ItemTappedEventArgs e)
        {
            if (!(e.Item is OrderView orderView))
                return;

            await Navigation.PushAsync(new OrderDetailsPage(orderView.Order));
        }
    }
}