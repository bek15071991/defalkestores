﻿using DefalkeStores.Api.Dto;
using DefalkeStores.Core;

namespace DefalkeStores.Application
{
    public static class ProductExtension
    {
        public static void UpdateFrom(this Product product, ProductDto productDto, Category category)
        {
            product.Set(productDto.Name,
                productDto.Cost,
                productDto.Price,
                productDto.Count,
                productDto.Description,
                productDto.Image, 
                category);
        }
    }
}