﻿using System;
using System.Threading.Tasks;

namespace Commons.Forms
{
   public  static class ApplicationExceptionHandler
    {
        public static void HandleException(this AppDomain appDomain)
        {    
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => ShowException((Exception)args.ExceptionObject);
            TaskScheduler.UnobservedTaskException += (sender, args) => ShowException(args.Exception);
        }

        private static void ShowException(Exception ex)
        {
           
             
        }
    }
}
