﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Commons.DataAccess;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;
using Microsoft.AspNetCore.Mvc;

namespace DefalkeStores.Api.Orders
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICourierSettingsProvider _courierSettingsProvider;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrdersController(IOrderRepository orderRepository,
            IProductRepository productRepository,
            IUserRepository userRepository,
            ICourierSettingsProvider courierSettingsProvider,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _courierSettingsProvider = courierSettingsProvider ??
                                       throw new ArgumentNullException(nameof(courierSettingsProvider));
            _unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] int count,
            int skip = 0,
            int? userId = null,
            OrderStatus? orderStatus = null)
        {
            var orders = await _orderRepository.Get(count, skip, userId, orderStatus);
            var response = orders?.Select(o => new OrderDto
            {
                Id = o.Id,
                User = new UserDto
                {
                    Id = o.UserId,
                    Name = o.User.Name,
                    Phone = o.User.Phone
                },
                DestinationAddress = o.DestinationAddress,
                DestinationName = o.DestinationName,
                DestinationPhone = o.DestinationPhone,
                Postcard = o.Postcard,
                Number = o.Number,
                OnDate = o.OnDate,
                Status = (int) o.Status,
                CourierCost = o.CourierCost,
                PostcardText = o.PostcardText,
                Total = o.Total,
                Lines = o.Lines.Select(l => new OrderLineDto
                {
                    Count = l.Count,
                    ProductId = l.ProductId,
                    Price = l.Product.Price,
                    ProductName = l.Product.Name,
                    ProductThumbnailImage = l.Product.ThumbnailImage.ToList()
                }).ToList()
            });

            return Ok(response);
        }

        [HttpGet("active")]
        public async Task<IActionResult> GetActive([FromQuery] int count, int skip = 0)
        {
            var orders = await _orderRepository.GetActive(count, skip);
            var response = orders?.Select(o => new OrderDto
            {
                Id = o.Id,
                User = new UserDto
                {
                    Id = o.UserId,
                    Name = o.User.Name,
                    Phone = o.User.Phone
                },
                DestinationAddress = o.DestinationAddress,
                DestinationName = o.DestinationName,
                DestinationPhone = o.DestinationPhone,
                Postcard = o.Postcard,
                Number = o.Number,
                OnDate = o.OnDate,
                Status = (int) o.Status,
                CourierCost = o.CourierCost,
                PostcardText = o.PostcardText,
                Total = o.Total,
                Lines = o.Lines.Select(l => new OrderLineDto
                {
                    Count = l.Count,
                    ProductId = l.ProductId,
                    Price = l.Product.Price,
                    ProductName = l.Product.Name,
                    ProductThumbnailImage = l.Product.ThumbnailImage.ToList()
                }).ToList()
            });

            return Ok(response);
        }

        [HttpPost("")]
        public async Task<IActionResult> MakeOrder([FromBody] OrderDto orderDto)
        {
            if (orderDto?.User == null)
                return BadRequest();

            var userDto = orderDto.User;

            var user = await _userRepository.Find(userDto.Id);

            if (userDto.Id == 0 || user == null)
            {
                user = new User(userDto.Name, userDto.Phone);
                await _userRepository.Save(user);
            }

            if (!OrderGuard.Validate(orderDto))
                return BadRequest();

            var courierSettings = await _courierSettingsProvider.Get();

            var order = new Order(user,
                orderDto.DestinationAddress,
                orderDto.DestinationName,
                orderDto.DestinationPhone,
                courierSettings.Cost,
                orderDto.Postcard,
                orderDto.PostcardText);

            foreach (var orderLineDto in orderDto.Lines)
            {
                var product = await _productRepository.Find(orderLineDto.ProductId);

                if (product == null)
                    return BadRequest();

                order.AddLine(product, orderLineDto.Count);
            }

            await _orderRepository.Save(order);

            orderDto.User.Id = user.Id;
            orderDto.Id = order.Id;

            return Ok(orderDto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStatus(int id, [FromBody] OrderUpdateStatus model)
        {
            if (model == null)
                return BadRequest();

            using (var uow = _unitOfWorkFactory.Create())
            {
                var order = await _orderRepository.Find(id);

                if (order == null)
                    return BadRequest();

                order.UpdateStatus(model.Status);

                await _orderRepository.Save(order); 

                await uow.Commit();
            }

            return Ok();
        }
    }
}