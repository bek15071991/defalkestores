using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using Autofac;
using DefalkeStores.Api.Infrastructure;
using DefalkeStores.Application;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using IHostingEnvironment = Microsoft.Extensions.Hosting.IHostingEnvironment;

namespace DefalkeStores.Api
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; private set; }

        public ILifetimeScope AutofacContainer { get; private set; }

        public Serilog.ILogger Logger { get; }

        public Startup(IHostingEnvironment env)
        { 
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables(); 

            Configuration = builder.Build();

            Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .Destructure.AsScalar<JObject>()
                .Destructure.AsScalar<JArray>()
                .Enrich.FromLogContext()
                .CreateLogger();

            Logger.Information("Starting DefalkeSrores.Api");
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddControllers();

            var connection = Configuration.GetConnectionString("DefalkeStoresConnection");

            services.AddDbContext<StoresContext>(options => options.UseSqlServer(connection));

            services.AddSingleton(Logger);
             
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new ApplicationModule());
        }

        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            ILoggerFactory loggerFactory)
        {   
            loggerFactory.AddSerilog();

            app.UseMiddleware<RequestLoggingMiddleware>();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();

            app.UseExceptionHandler(c => c.Run(async context =>
            {
                var exception = context.Features
                                       .Get<IExceptionHandlerPathFeature>()
                                       .Error;

                Logger.Error(exception, "Request error");

                await context.Response.WriteAsync("");
            }));

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); }); 
        }
    }
}