﻿namespace DefalkeStores.Api.Dto
{
    public enum OrderStatus
    {
        Accepted = 1, // Принят
        Making, // Готовится
        Sended, // Отправлен
        Delivered, // Доставлен
        Canceled,  // Отменен
        PaymentPending // Ожидается платеж
    }
}