﻿using DefalkeStores.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DefalkeStores.Application.Configurations
{
    class ProductConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasOne(f => f.OriginalImage)
                .WithOne(f => f.Product)
                .HasForeignKey<ProductOriginalImage>(f => f.ProductId);
        }
    }
}