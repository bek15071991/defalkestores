﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
   public interface ICategoryRepository : IDeleteRepository<Category>, ISaveRepository<Category>
   {
       Task<IList<Category>> Get(string? name, int count);
       Task<IList<Category>> GetRoot(); 
       Task<Category> Find(int id);

       Task<IList<Category>> GetChildren(int id);
   }
}
