﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using DefalkeStores.Api.Dto;
using DefalkeStores.Core;

namespace DefalkeStores.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourierController : ControllerBase
    {
        private readonly ICourierSettingsProvider _courierSettingsProvider;

        public CourierController(ICourierSettingsProvider courierSettingsProvider)
        {
            _courierSettingsProvider = courierSettingsProvider ??
                                       throw new ArgumentNullException(nameof(courierSettingsProvider));
        }

        [HttpGet("settings")]
        public async Task<IActionResult> GetSettings()
        {
            var settings = await _courierSettingsProvider.Get(); 

            return Ok(new CourierSettingsDto
            {
                Cost = settings.Cost,
                PostcardCost = settings.PostcardCost
            });
        }
    }
}