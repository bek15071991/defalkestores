﻿using System.Threading.Tasks;

namespace DefalkeStores.Core
{
   public interface ICourierSettingsProvider
   {
       Task<CourierSettings> Get();
   }
}
