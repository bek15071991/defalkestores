﻿using System;
using System.Globalization;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace Commons.Forms.Converters
{
   public class OrderStatusConverter : IValueConverter
   {
       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           if (value == null)
               return null;

           var status = (OrderStatus)value;

           switch (status)
           {
                case OrderStatus.Accepted: return "Принят";
                case OrderStatus.Making: return "Готовится";
                case OrderStatus.Sended: return "Отправлен";
                case OrderStatus.Delivered: return "Доставлен";
                case OrderStatus.Canceled: return "Отменен";
                case OrderStatus.PaymentPending: return "Ожидается платеж";

                default: return status;
           } 
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           throw new NotImplementedException();
       }
   }
}
