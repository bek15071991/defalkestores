﻿using System;
using System.Threading.Tasks;
using Autofac;
using Commons;
using Commons.Forms;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Banners
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BannerPage
    {
        public BannerPage()
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<BannerViewModel>();
            BindingContext = viewModel;

            viewModel.LoadCommand.Execute(null);
        }

        private static async Task<byte[]> GetSelectedPhoto()
        {
            var result = await MediaPicker.PickPhotoAsync(new MediaPickerOptions
            {
                Title = "Выберите фото"
            });

            var imageStream = await result.OpenReadAsync();

            return imageStream.ToArray();
        }

        private async void Photo1_Selected(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<BannerViewModel>();

            viewModel.Image1 = await GetSelectedPhoto();
        }

        private async void Photo2_Selected(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<BannerViewModel>();

            viewModel.Image2 = await GetSelectedPhoto();
        }

        private async void Photo3_Selected(object sender, EventArgs e)
        {
            var viewModel = this.GetVieModel<BannerViewModel>();

            viewModel.Image3 = await GetSelectedPhoto();
        }
    }
}