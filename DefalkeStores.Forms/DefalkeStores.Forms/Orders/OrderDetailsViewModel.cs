﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto; 
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Orders
{
    public class OrderDetailsViewModel : ViewModel
    {
         
        private string _destinationName; 
        public string DestinationName
        {
            get => _destinationName;
            set => SetProperty(ref _destinationName, value);
        }

        private string _destinationAddress; 
        public string DestinationAddress
        {
            get => _destinationAddress;
            set => SetProperty(ref _destinationAddress, value);
        }

        private string _destinationNumber; 
        public string DestinationNumber
        {
            get => _destinationNumber;
            set => SetProperty(ref _destinationNumber, value);
        }

        private bool _includePostCard; 
        public bool IncludePostCard
        {
            get => _includePostCard;
            set => SetProperty(ref _includePostCard, value);
        }

        private string? _postCardText; 
        public string? PostCardText
        {
            get => _postCardText;
            set => SetProperty(ref _postCardText, value);
        }

        private int _postCardCost; 
        public int PostCardCost
        {
            get => _postCardCost;
            set => SetProperty(ref _postCardCost, value);
        }

        private string _senderNumber; 
        public string SenderNumber
        {
            get => _senderNumber;
            set => SetProperty(ref _senderNumber, value);
        }

        private string _senderName; 
        public string SenderName
        {
            get => _senderName;
            set => SetProperty(ref _senderName, value);
        }

        private OrderStatus _selectedStatus;

        public OrderStatus SelectedStatus
        {
            get => _selectedStatus;
            set => SetProperty(ref _selectedStatus, value);
        }


        private int _amount;

        public int Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private int _courierCost;

        public int CourierCost
        {
            get => _courierCost;
            set => SetProperty(ref _courierCost, value);
        }

        private int _total;

        public int Total
        {
            get => _total;
            set => SetProperty(ref _total, value);
        }

        public ObservableCollection<OrderLineDto> Lines { get; set; }

        public List<OrderStatus> Statuses { get; } = new List<OrderStatus>
        {
            OrderStatus.Accepted, // Принят
            OrderStatus.Making, // Готовится
            OrderStatus.Sended, // Отправлен
            OrderStatus.Delivered, // Доставлен
            OrderStatus.Canceled,  // Отменен
            OrderStatus.PaymentPending // Ожидается платеж
        };

        public ICommand LoadCommand { get; }
        public ICommand SaveCommand { get; }

        private int _id;

        private readonly IOrderClient _orderClient;
        private readonly IMessageBoxService _messageBoxService;

        public OrderDetailsViewModel(IOrderClient orderClient, IMessageBoxService messageBoxService)
        {
            _orderClient = orderClient ?? throw new ArgumentNullException(nameof(orderClient));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            LoadCommand = new Command<OrderDto>(Load);
            SaveCommand = new Command(Save, () => !IsBusy);
        }

        private void Load(OrderDto order)
        {
            if (order == null) throw new ArgumentNullException(nameof(order)); 
          
            _id = order.Id.Value;

            Title = order.Number;
            SelectedStatus = (OrderStatus) order.Status.Value;
            DestinationName = order.DestinationName;
            DestinationAddress = order.DestinationAddress;
            DestinationNumber = order.DestinationPhone;
            IncludePostCard = order.Postcard != null;
            PostCardText = order.PostcardText;
            PostCardCost = order.Postcard.GetValueOrDefault();

            Lines = new ObservableCollection<OrderLineDto>(order.Lines);

            SenderName = order.User.Name;
            SenderNumber = order.User.Phone;

            Amount = Lines.Sum(s => s.Price.GetValueOrDefault());
            Total = order.Total.GetValueOrDefault();
            CourierCost = order.CourierCost.GetValueOrDefault();
        }

        private async void Save()
        {
            try
            {
                IsBusy = true;

               await _orderClient.UpdateStatus(_id, SelectedStatus);

               MessagingCenter.Send(this, MessageCenterActions.ChangedOrderStatus, _id);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
                await _messageBoxService.Notify("Возникла ошибка при сохранении");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}