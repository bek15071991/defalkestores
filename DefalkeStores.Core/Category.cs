﻿using System;
using System.Collections.Generic;
using Commons.DataAccess;

namespace DefalkeStores.Core
{
    public class Category : IEntity
    {
        public int Id { get; protected set; }

        public string Name { get; protected set; }

        public virtual Category? Parent { get; protected set; }

        public int? ParentId { get; protected set; }

        public virtual IList<Category>? Children { get; protected set; }

        protected Category()  // for ef
        {
            
        }

        public Category(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public Category(string name, Category parent) : this(name)
        {
            Parent = parent ?? throw new ArgumentNullException(nameof(parent));
            ParentId = parent.Id;
        }

        public void SetName(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public void SetParent(Category parent)
        {
            Parent = parent ?? throw new ArgumentNullException(nameof(parent));
            ParentId = parent.Id;
        }
    }
}