﻿using System;
using System.Threading.Tasks;
using Commons.DataAccess;
using Microsoft.EntityFrameworkCore.Storage;

namespace DefalkeStores.Application
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _isCommit;
        private readonly IDbContextTransaction _transaction;

        public UnitOfWork(StoresContext storesContext)
        {
            var storesContext1 = storesContext ?? throw new ArgumentNullException(nameof(storesContext));

            _transaction = storesContext1.Database.BeginTransaction();
        }

        public Task Commit()
        {
            _isCommit = true;
            return _transaction.CommitAsync();
        }

        public void Dispose()
        {
            if (_isCommit)
                return;

            _transaction.Rollback();
            _transaction?.Dispose();
        }
    }
}