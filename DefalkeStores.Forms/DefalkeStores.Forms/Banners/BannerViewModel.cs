﻿using System;
using System.Windows.Input;
using Commons.Forms;
using DefalkeStores.Api.Client;
using DefalkeStores.Api.Dto;
using Xamarin.Forms;

namespace DefalkeStores.Forms.Banners
{
    public class BannerViewModel : ViewModel
    {
        public string Name { get; set; }

        private string _text;

        public string Text
        {
            get => _text;
            set => SetProperty(ref _text, value);
        }

        private byte[] _image1;

        public byte[] Image1
        {
            get => _image1;
            set => SetProperty(ref _image1, value);
        }

        private byte[] _image2;

        public byte[] Image2
        {
            get => _image2;
            set => SetProperty(ref _image2, value);
        }

        private byte[] _image3;

        public byte[] Image3
        {
            get => _image3;
            set => SetProperty(ref _image3, value);
        }

        public ICommand LoadCommand { get; }
        public ICommand SaveCommand { get; }

        private readonly IBannerClient _bannerClient;
        private readonly IMessageBoxService _messageBoxService;

        public BannerViewModel(IBannerClient bannerClient, IMessageBoxService messageBoxService)
        {
            _bannerClient = bannerClient ?? throw new ArgumentNullException(nameof(bannerClient));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            SaveCommand = new Command(Save, () => !IsBusy);
            LoadCommand = new Command(Load);

            Title = "Баннер реклама";
        }

        private async void Load()
        {
            try
            {
                IsBusy = true;

                var banner = await _bannerClient.GetMain();

                Name = banner.Name;
                Text = banner.Text;
                Image1 = banner.Image1;
                Image2 = banner.Image2;
                Image3 = banner.Image3;
            }
            catch (Exception e)
            {
                await _messageBoxService.Notify("Невозможно получить данные", "Ошибка");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async void Save()
        {
            try
            {
                var banner = new BannerDto
                {
                    Name = Name,
                    Text = Text,
                    Image1 = Image1,
                    Image2 = Image2,
                    Image3 = Image3
                };

                IsBusy = true;

                await _bannerClient.Save(banner);

                await _messageBoxService.Notify("Реклама сохранена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}