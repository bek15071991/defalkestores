﻿using Autofac; 
using DefalkeStores.Forms.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DefalkeStores.Forms.Categories
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubCategoryPage : ContentPage
    {
        public SubCategoryPage(int parentId, string parentName)
        {
            InitializeComponent();

            using var scope = App.Container.BeginLifetimeScope();
            var viewModel = scope.Resolve<SubCategoryViewModel>();

            BindingContext = viewModel;

            viewModel.Load(parentId, parentName);

            MessagingCenter.Subscribe<SubCategoryViewModel>(this, MessageCenterActions.AddedSubCategory,
                async view =>
                {
                    await Navigation.PopAsync(true);
                });
        }
    }
}