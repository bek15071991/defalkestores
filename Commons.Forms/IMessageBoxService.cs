﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Commons.Forms
{
    public interface IMessageBoxService
    {
        Task Notify(string text, string title = "...");

        Task<bool> Ask(string text, string title);
    }

    public class MessageBoxService : IMessageBoxService
    {
        public async Task Notify(string text, string title)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text));

            await Application.Current.MainPage.DisplayAlert(title, text, "Ok");
        }

        public async Task<bool> Ask(string text, string title)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text));

            if (title == null) 
                throw new ArgumentNullException(nameof(title));

            return await Application.Current.MainPage.DisplayAlert(title, text, "Ок", "Нет");
        }
    }
}