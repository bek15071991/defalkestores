﻿using System.IO;
using System.Threading.Tasks;
using DefalkeStores.Forms.iOS;
using DefalkeStores.Forms.Products;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(PhotoPickerService))]
namespace DefalkeStores.Forms.iOS
{
    public class PhotoPickerService : IPhotoPickerService
    {
        TaskCompletionSource<ProductImage> taskCompletionSource;
        UIImagePickerController imagePicker;

        public Task<ProductImage> GetImageAsync()
        {
            // Create and define UIImagePickerController
            imagePicker = new UIImagePickerController
            {
                SourceType = UIImagePickerControllerSourceType.PhotoLibrary,
                MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary)
            };

            // Set event handlers
            //imagePicker.FinishedPickingMedia += OnImagePickerFinishedPickingMedia;
            //imagePicker.Canceled += OnImagePickerCancelled;

            // Present UIImagePickerController;
            UIWindow window = UIApplication.SharedApplication.KeyWindow;
            var viewController = window.RootViewController;
            viewController.PresentViewController(imagePicker, true, null);

            // Return Task object
            taskCompletionSource = new TaskCompletionSource<ProductImage>();
            return taskCompletionSource.Task;
        }
    }
}